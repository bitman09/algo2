#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cstdlib>

#include "cmdline.h"
#include "FileCruncher.h"


using namespace std;

////////////////////////////////////////////////////////////////////////////
//




////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////
//FUNCTION PROTOTYPES

static void opt_input(string const &);
static void opt_output(string const &);
static void opt_help(string const &);


void print_network_info(string &s, size_t hubs, size_t nodes, size_t amps, size_t cmodems, size_t tot_connections);


////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////
//GLOBAL VARIABLES




static option_t options[] = {
	{1, "i", "input", "-", opt_input, OPT_DEFAULT},
	{1, "o", "output", "-", opt_output, OPT_DEFAULT},
	{0, "h", "help", NULL, opt_help, OPT_DEFAULT},
	{0, },//Indica el final del arreglo
};
static istream *iss = 0; // puntero a: cin o al archivo de entrada
static string isName;    // Nombre del archivo de entrada
static ostream *oss = 0; // puntero a: cout o al archivo de salida
static fstream ifs; 
static fstream ofs;



////////////////////////////////////////////////////////////////////////////


int main(int argc, char *argv[])
{

	cmdline cmdl(options); //creo el objeto cmdl
	cmdl.parse(argc, argv); // parse de los argumentos de línea de comando

	FileCruncher p(*iss, isName);
	try {
		p.parse();
	}
	catch(exception &e) {
			cerr << "error: " << e.what() << endl;
			return 1;
	}

	string  netName(p.getNetworkName());
	print_network_info(	netName,
    					(size_t) p.getNumHubs(),
    					(size_t) p.getNumNodes(),
    					(size_t) p.getNumAmps(),
    					(size_t) p.getNumCMs(),
    					(size_t) p.getNumConnections()
    					);

	return 0;
}



////////////////////////////////////////////////////////////////////////////

static void opt_input(string const &arg)
{
	// Si el nombre del archivos es "-", usaremos la entrada
	// estándar. De lo contrario, abrimos un archivo en modo
	// de lectura.
	//
	if (arg == "-") {
		iss = &cin;
	} else {
		ifs.open(arg.c_str(), ios::in);
		iss = &ifs;
	}
	isName = arg;

	// Verificamos que el stream este OK.
	//
	if (!iss->good()) {
		cerr << "cannot open "
		     << arg
		     << "."
		     << endl;
		exit(1);
	}
}

static void opt_output(string const &arg)
{
	// Si el nombre del archivos es "-", usaremos la salida
	// estándar. De lo contrario, abrimos un archivo en modo
	// de escritura.
	//
	if (arg == "-") {
		oss = &cout;
	} else {
		ofs.open(arg.c_str(), ios::out);
		oss = &ofs;
	}

	// Verificamos que el stream este OK.
	//
	if (!oss->good()) {
		cerr << "cannot open "
		     << arg
		     << "."
		     << endl;
		exit(1);
	}
}

static void opt_help(string const &arg)
{
	cout << "tp0.exe [-i file] [-o file]"
	     << endl;
	exit(0);
}

////////////////////////////////////////////////////////////////////////////


void print_network_info(string &s, size_t hubs, size_t nodes, size_t amps, size_t cmodems, size_t tot_connections)
{
	// oss es un PUNTERO istream definido GLOBAL. Apunta al flujo de salida. 
	
	*oss << s << endl;
	*oss << hubs << " hub" << endl;
	*oss << nodes << " nodes" << endl;
	*oss << amps << " amplifiers" << endl;
	*oss << cmodems << " cable modems " << endl;
	*oss << tot_connections << " connections " << endl;

}


////////////////////////////////////////////////////////////////////////////



