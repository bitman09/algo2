/*
 * FileCruncher.h
 *
 *  Created on: Mar 23, 2014
 */

#ifndef FILECRUNCHER_H_
#define FILECRUNCHER_H_

#include <iostream>
#include <exception>
#include <sstream>
#include <cstring>
#include <set>

#include "LineParser.h"
#include "Exceptions.h"

using namespace std;

class FileCruncher {
	friend class LineParserTestSuite;
private:
	istream *in;
	string fileName;
	unsigned int linea;
	string networkName;
	unsigned int numConnectionElements;
	unsigned int numHubs;
	unsigned int numNodes;
	unsigned int numCM;
	unsigned int numAmp;

	set<string> elemNames;

	/**
	 * Estado de parsing del archivo
	 *
	 * - 0 : no iniciado
	 * - 1 : NetworkName
	 * - 2 : Networkelement
	 * - 3 : Connection
	 */
	unsigned int parsingState;

	static const int NETWORKNAME_NUM_PARAMS = 1;
	static const int ELEMENT_OR_CONNECTION_PARAM_SIZE = 2;
	static const char* CMD_NETWORK_NAME;
	static const char* CMD_CONNECTION;
	static const char* CMD_NETWORK_ELEMENT;

	class LineParser {
	private:
		string line;
		string command;
		vector<string> *params;

	public:
		LineParser(const string );
		LineParser(const LineParser & );
		virtual ~LineParser();
		void parse();
		string getCommando() const;
		vector<string> * getParams() const;
	};

	void readNetworkName() throw(exception);
	void readNetworkElements() throw(exception);
	void validarComando( const LineParser & ) const throw(exception);
	void validarArgumentos( const LineParser & ) const throw(exception);
	void estaDeclaradoValidar( const string ) const throw(exception);
	void validarNetworkName( const LineParser  & ) const;

public:
	FileCruncher(istream &, const string);
	FileCruncher(const FileCruncher & );
	virtual ~FileCruncher();
	void parse() throw (exception);
	string getNetworkName() const;
	unsigned int getNumConnections() const;
	unsigned int getNumHubs() const;
	unsigned int getNumNodes() const;
	unsigned int getNumCMs() const;
	unsigned int getNumAmps() const;
};

#endif /* FILECRUNCHER_H_ */
