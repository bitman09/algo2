/*
 * Exceptions.h
 *
 *  Created on: Apr 7, 2014
 */

#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <exception>
#include <string>
#include <sstream>
#include <cstring>
using namespace std;

class comandoIncorrecto: public exception {
private:
	string cmd;
	string expected;
	string filename;
	unsigned int linea;
	mutable char * whatCStr;
public:
	comandoIncorrecto(string , unsigned int, string );
	comandoIncorrecto(string , string, unsigned int, string );
    const char * what() const throw ();
	virtual ~comandoIncorrecto() throw();
};

class comandoNoEsperado: public exception {
private:
	string cmd;
	unsigned int linea;
	mutable char * whatCStr;
	string filename;
public:
	comandoNoEsperado(string , unsigned int, string );
	virtual const char * what() const throw ();
	virtual ~comandoNoEsperado() throw();
};

class paramsInvalidos: public exception {
private:
	string cmd;
	unsigned int linea;
	unsigned int expected;
	unsigned int obtained;
	mutable char * whatCStr;
	string filename;
public:
	paramsInvalidos(string, unsigned int, unsigned int, unsigned int, string);
	virtual const char * what() const throw ();
	virtual ~paramsInvalidos() throw();
};

class tipoNetowrkelementIncorrecto: public exception {
private:
	string tipo;
	unsigned int linea;
	mutable char * whatCStr;
	string filename;
public:
	tipoNetowrkelementIncorrecto(string , unsigned int, string );
	virtual const char * what() const throw ();
	virtual ~tipoNetowrkelementIncorrecto() throw();
};

class networkElementNoDeclarado: public exception {
private:
	string nombre;
	unsigned int linea;
	mutable char * whatCStr;
	string filename;
public:
	networkElementNoDeclarado(string , unsigned int, string );
	virtual const char * what() const throw ();
	virtual ~networkElementNoDeclarado() throw();
};


class networkElementDuplicado: public exception {
private:
	string nombre;
	unsigned int linea;
	mutable char * whatCStr;
	string filename;
public:
	networkElementDuplicado(string , unsigned int, string );
	virtual const char * what() const throw ();
	virtual ~networkElementDuplicado() throw();
};



#endif /* EXCEPTIONS_H_ */
