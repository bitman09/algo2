/*
 * Exceptions.cpp
 *
 *  Created on: Apr 7, 2014
 */

#include "Exceptions.h"

comandoIncorrecto::comandoIncorrecto(string cmd, unsigned int linea, string filename) {
	this->cmd = cmd;
	this->linea = linea;
	this->expected = string("");
	this->whatCStr = NULL;
	this->filename = filename;
}

comandoIncorrecto::comandoIncorrecto(string cmd, string expected, unsigned int linea, string filename) {
	this->cmd = cmd;
	this->linea = linea;
	this->expected = expected;
	this->whatCStr = NULL;
	this->filename = filename;
}

comandoIncorrecto::~comandoIncorrecto() throw () {
	if ( this->whatCStr != NULL)
		delete [] this->whatCStr;
}

const char * comandoIncorrecto::what() const throw () {
	ostringstream oss;
	oss << "unknown command: '" << this->cmd << "'";

	if (! this->expected.empty() )
		oss << " expected '" << this->expected << "'";

	oss << " at " << this->filename << ":" << this->linea;

	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());

	return this->whatCStr;
}

tipoNetowrkelementIncorrecto::tipoNetowrkelementIncorrecto(string tipo, unsigned int linea, string filename) {
	this->tipo = tipo;
	this->linea = linea;
	this->whatCStr = NULL;
	this->filename = filename;
}

tipoNetowrkelementIncorrecto::~tipoNetowrkelementIncorrecto() throw () {
	if ( this->whatCStr != NULL)
		delete [] whatCStr;
}

const char * tipoNetowrkelementIncorrecto::what() const throw () {
	ostringstream oss;
	oss << "unknown network type: " << this->tipo << " at " << this->filename << ":" << this->linea;

	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());
	return whatCStr;
}

networkElementDuplicado::networkElementDuplicado(string tipo, unsigned int linea, string filename) {
	this->nombre = tipo;
	this->linea = linea;
	this->whatCStr = NULL;
	this->filename = filename;
}

networkElementDuplicado::~networkElementDuplicado() throw () {
	if ( this->whatCStr != NULL)
		delete [] whatCStr;
}

const char * networkElementDuplicado::what() const throw () {
	ostringstream oss;
	oss << "duplicated network element '" << this->nombre << "' at " << this->filename << ":" << this->linea;

	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());
	return whatCStr;
}


networkElementNoDeclarado::networkElementNoDeclarado(string nombre, unsigned int linea, string filename) {
	this->nombre = nombre;
	this->linea = linea;
	this->whatCStr = NULL;
	this->filename = filename;
}

networkElementNoDeclarado::~networkElementNoDeclarado() throw () {
	if ( this->whatCStr != NULL)
		delete [] whatCStr;
}

const char * networkElementNoDeclarado::what() const throw () {
	ostringstream oss;
	oss << "undeclared network element '" << this->nombre << "' at " << this->filename << ":" << this->linea;

	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());
	return whatCStr;
}

comandoNoEsperado::comandoNoEsperado(string cmd, unsigned int linea, string filename) {
	this->cmd = cmd;
	this->linea = linea;
	this->whatCStr = NULL;
	this->filename = filename;
}

comandoNoEsperado::~comandoNoEsperado() throw () {
	if ( this->whatCStr != NULL)
		delete [] whatCStr;
}

const char * comandoNoEsperado::what() const throw () {
	ostringstream oss;
	oss << "unexpected command: " << this->cmd << " at " << this->filename << ":" << this->linea;

	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());
	return whatCStr;
}

paramsInvalidos::paramsInvalidos(string cmd, unsigned int linea, unsigned int expected, unsigned int obtained, string filename) {
	this->cmd = cmd;
	this->linea = linea;
	this->expected = expected;
	this->obtained = obtained;
	this->whatCStr = NULL;
	this->filename = filename;
}

paramsInvalidos::~paramsInvalidos() throw () {
	if ( this->whatCStr != NULL)
		delete [] whatCStr;
}

const char * paramsInvalidos::what() const throw () {
	ostringstream oss;
	oss << "command " << this->cmd << ": expected " << this->expected << "parameters but got ";
	oss << this->obtained << " at " << this->filename << ":" << this->linea;

	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());
	return whatCStr;
}

