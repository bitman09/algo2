/*
 * tests.cpp
 *
 *  Created on: Mar 27, 2014
 *      Author: vicrodri
 */

#include <cpptest.h>
#include "FileCruncher.h"

/**
 * Tests de LineParser
 */


class LineParserTestSuite: public Test::Suite {
public:
	LineParserTestSuite() {
		TEST_ADD(LineParserTestSuite::comandoUnico)
		TEST_ADD(LineParserTestSuite::comandoSinParams)
		TEST_ADD(LineParserTestSuite::comandoConEspacio)
		TEST_ADD(LineParserTestSuite::comandoConEspacioAlfinal)
		TEST_ADD(LineParserTestSuite::comandoParametro)
		TEST_ADD(LineParserTestSuite::comando2Parametros)
	}

private:
	void comandoUnico();
	void comandoSinParams();
	void comandoConEspacio();
	void comandoConEspacioAlfinal();
	void comandoParametro();
	void comando2Parametros();
};

void LineParserTestSuite::comandoUnico() {
	FileCruncher::LineParser *lp = new FileCruncher::LineParser("AAAA bbbbb");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommando());
}

void LineParserTestSuite::comandoSinParams() {
	FileCruncher::LineParser *lp = new FileCruncher::LineParser("AAAA");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommando());
}

void LineParserTestSuite::comandoConEspacio() {
	FileCruncher::LineParser *lp = new FileCruncher::LineParser(" AAAA");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommando());
}

void LineParserTestSuite::comandoConEspacioAlfinal() {
	FileCruncher::LineParser *lp = new FileCruncher::LineParser("AAAA ");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommando());
}

void LineParserTestSuite::comandoParametro() {
	FileCruncher::LineParser *lp = new FileCruncher::LineParser("AAAA bbbbb");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommando());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(1, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
}

void LineParserTestSuite::comando2Parametros() {
	FileCruncher::LineParser *lp = new FileCruncher::LineParser("AAAA bbbbb ccc");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommando());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(2, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	TEST_ASSERT_EQUALS("ccc", (*p)[1]);
}

/**
 * Tests de FileParser
 */
class FileParserTestSuite: public Test::Suite {
public:
	FileParserTestSuite() {
		TEST_ADD(FileParserTestSuite::networkName)
		TEST_ADD(FileParserTestSuite::networkNameMal)
		TEST_ADD(FileParserTestSuite::networkNameMalParams)
		TEST_ADD(FileParserTestSuite::networkElement)
		TEST_ADD(FileParserTestSuite::networkElementMal)
		TEST_ADD(FileParserTestSuite::networkElementTipoInvalido)
		TEST_ADD(FileParserTestSuite::networkElement2)
		TEST_ADD(FileParserTestSuite::networkElementCountHubs)
		TEST_ADD(FileParserTestSuite::networkElementCountAmps)
		TEST_ADD(FileParserTestSuite::networkElementCountCMs)
		TEST_ADD(FileParserTestSuite::networkElementCountNodes)
		TEST_ADD(FileParserTestSuite::networkElementEstaDuplicado)
		TEST_ADD(FileParserTestSuite::connection)
		TEST_ADD(FileParserTestSuite::connectionMal)
		TEST_ADD(FileParserTestSuite::connection2)
		TEST_ADD(FileParserTestSuite::connectionMix)
		TEST_ADD(FileParserTestSuite::connectionCmdNoExiste)
		TEST_ADD(FileParserTestSuite::connectionCmdFueraDeOrden)
		TEST_ADD(FileParserTestSuite::connectionNewtorkElementNoDeclarado)
	}

private:
	void networkName();
	void networkNameMal();
	void networkNameMalParams();
	void networkElement();
	void networkElementMal();
	void networkElementTipoInvalido();
	void networkElement2();
	void networkElementCountHubs();
	void networkElementCountAmps();
	void networkElementCountCMs();
	void networkElementCountNodes();
	void networkElementEstaDuplicado();
	void connection();
	void connectionMal();
	void connection2();
	void connectionMix();
	void connectionCmdNoExiste();
	void connectionCmdFueraDeOrden();
	void connectionNewtorkElementNoDeclarado();
};

void FileParserTestSuite::networkNameMal() {

	string inS = "NetworkNameMAL pepito";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");

	TEST_THROWS(fp->parse(), comandoIncorrecto);

}


void FileParserTestSuite::networkNameMalParams() {

	string inS = "NetworkName pepito tiene hambre";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");

	TEST_THROWS(fp->parse(), paramsInvalidos);

}

void FileParserTestSuite::networkName() {

	string inS = "NetworkName pepito";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
}

void FileParserTestSuite::networkElement() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(1, fp->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp->getNumConnections())
}

void FileParserTestSuite::networkElementMal() {

	string inS = "NetworkName pepito\nNetworkElementMal H1 Hub";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), comandoIncorrecto)
}

void FileParserTestSuite::networkElement2() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(2, fp->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp->getNumConnections())
}

void FileParserTestSuite::networkElementCountHubs() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(1, fp->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp->getNumAmps())
	TEST_ASSERT_EQUALS(0, fp->getNumCMs())
	TEST_ASSERT_EQUALS(0, fp->getNumNodes())
	TEST_ASSERT_EQUALS(0, fp->getNumConnections())
}

void FileParserTestSuite::networkElementCountAmps() {

	string inS = "NetworkName pepito\nNetworkElement H1 Amp";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(0, fp->getNumHubs())
	TEST_ASSERT_EQUALS(1, fp->getNumAmps())
	TEST_ASSERT_EQUALS(0, fp->getNumCMs())
	TEST_ASSERT_EQUALS(0, fp->getNumNodes())
	TEST_ASSERT_EQUALS(0, fp->getNumConnections())
}


void FileParserTestSuite::networkElementCountNodes() {

	string inS = "NetworkName pepito\nNetworkElement H1 Node";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(0, fp->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp->getNumAmps())
	TEST_ASSERT_EQUALS(0, fp->getNumCMs())
	TEST_ASSERT_EQUALS(1, fp->getNumNodes())
	TEST_ASSERT_EQUALS(0, fp->getNumConnections())
}

void FileParserTestSuite::networkElementEstaDuplicado() {

	string inS = "NetworkName pepito\nNetworkElement H1 Node\nNetworkElement H1 Node";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), networkElementDuplicado)
}


void FileParserTestSuite::networkElementCountCMs() {

	string inS = "NetworkName pepito\nNetworkElement H1 CM";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(0, fp->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp->getNumAmps())
	TEST_ASSERT_EQUALS(1, fp->getNumCMs())
	TEST_ASSERT_EQUALS(0, fp->getNumNodes())
	TEST_ASSERT_EQUALS(0, fp->getNumConnections())
}

void FileParserTestSuite::networkElementTipoInvalido() {

	string inS = "NetworkName pepito\nNetworkElement H1 TIPO_INVALIDO";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), tipoNetowrkelementIncorrecto);
}

void FileParserTestSuite::connection() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub\nConnection H1 H2";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(2, fp->getNumHubs())
	TEST_ASSERT_EQUALS(1, fp->getNumConnections())
}

void FileParserTestSuite::connectionMal() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub\nConnectionMal H1 H2";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), comandoIncorrecto)
}

void FileParserTestSuite::connection2() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub\nConnection H1 H2\nConnection H1 H2";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	fp->parse();
	TEST_ASSERT_EQUALS("pepito", fp->getNetworkName())
	TEST_ASSERT_EQUALS(2, fp->getNumHubs())
	TEST_ASSERT_EQUALS(2, fp->getNumConnections())
}


void FileParserTestSuite::connectionMix() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub\nConnection H1 H2\nNetworkElement H3 Hub";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), comandoNoEsperado);
}

void FileParserTestSuite::connectionCmdNoExiste() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub\nConnection H1 H2\nXXXXXXXXX H3 Hub";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), comandoIncorrecto);
}

void FileParserTestSuite::connectionCmdFueraDeOrden() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkName OtraVezPepito";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), comandoNoEsperado);
}

void FileParserTestSuite::connectionNewtorkElementNoDeclarado() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement H2 Hub\nConnection H1 H3";

	stringstream inSs(inS);
	FileCruncher *fp = new FileCruncher(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp->parse(), networkElementNoDeclarado);
}


int main(int argc, char *argv[]) {
	Test::TextOutput output(Test::TextOutput::Verbose);
	LineParserTestSuite ets;
	ets.run(output);
	FileParserTestSuite fpts;
	fpts.run(output);
}
