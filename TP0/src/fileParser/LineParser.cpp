/*
 * LineParser.cpp
 *
 *  Created on: Mar 23, 2014
 */


#include "FileCruncher.h"
#include "LineParser.h"

FileCruncher::LineParser::LineParser(const string line) {
	this->line = line;
	this->params = new vector<string>();
	this->command = "";
}

FileCruncher::LineParser::LineParser(const LineParser & lp) {
	this->line = lp.line;
	this->params = new vector<string>(*lp.params);
	this->command = lp.command;
}

FileCruncher::LineParser::~LineParser() {
	if (this->params != NULL)
		delete this->params;
}

void FileCruncher::LineParser::parse(){
	stringstream ss(this->line);

	ss >> this->command;

	string s;
	while( ! ss.eof() && ss.good() ) {
		ss >> s;
		this->params->push_back(s);
	}

}

vector<string> *FileCruncher::LineParser::getParams() const {
	return this->params;
}

string FileCruncher::LineParser::getCommando() const {
	return this->command;
}


