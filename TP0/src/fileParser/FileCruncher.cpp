/*
 * FileCruncher.cpp
 *
 *  Created on: Mar 23, 2014
 */

#include "FileCruncher.h"

const char* FileCruncher::CMD_NETWORK_NAME = "NetworkName";
const char* FileCruncher::CMD_CONNECTION = "Connection";
const char* FileCruncher::CMD_NETWORK_ELEMENT = "NetworkElement";

FileCruncher::FileCruncher(istream &i, const string fileName) {
	in = &i;
	this->fileName = fileName;
	this->linea = 1;
	this->networkName = "";

	this->numConnectionElements = 0;
	this->numHubs = 0;
	this->numNodes = 0;
	this->numCM = 0;
	this->numAmp = 0;

	this->elemNames = set<string>();

	this->parsingState = 0; // parsing no comenzado
}


FileCruncher::FileCruncher(const FileCruncher & fp ) {
	this->linea = fp.linea;
	this->networkName = fp.networkName;
	this->in = fp.in;
	this->fileName = fp.fileName;
	this->numConnectionElements = fp.numConnectionElements;
	this->numHubs = fp.numHubs;
	this->numNodes = fp.numNodes;
	this->numCM = fp.numCM;
	this->numAmp = fp.numAmp;
	this->elemNames = fp.elemNames;
	this->parsingState = fp.parsingState;
}

FileCruncher::~FileCruncher() {}

void FileCruncher::parse() throw (exception) {
	readNetworkName();
	if ( ! in->eof() ) {
		readNetworkElements();
	}
}

void FileCruncher::validarComando( const LineParser & lp) const throw(exception) {

	string cmd(lp.getCommando());

	// Valida que sean comandos conocidos
	if (cmd != CMD_NETWORK_ELEMENT && cmd != CMD_CONNECTION
			&& cmd != CMD_NETWORK_NAME) {
		throw comandoIncorrecto(cmd, this->linea, this->fileName);
	}
	// En esta fase del parsing no tiene cabida el comando NetworkName
	if (cmd == CMD_NETWORK_NAME) {
		throw comandoNoEsperado(cmd, this->linea, this->fileName);
	}
	// Esperamos un comando Connection
	if (this->parsingState == 3 && cmd == CMD_NETWORK_ELEMENT) {
		throw comandoNoEsperado(cmd, this->linea, this->fileName);
	}
}

void FileCruncher::readNetworkElements() throw(exception) {

	this->parsingState = 2;

	while (! in->eof()) {

		string line;
		getline(*in, line);

		if (line.empty() && in->eof())
			break;

		LineParser lp(line);
		lp.parse();

		validarComando(lp);
		validarArgumentos(lp);

		// A partir de este punto ya tenemos la línea validada

		string cmd(lp.getCommando());

		// Cambia de estado si se pasa de leer Networkelement a Connection
		if (this->parsingState == 2 && cmd == CMD_CONNECTION) {
			this->parsingState = 3;
		}

		if (cmd == CMD_NETWORK_ELEMENT) {
			string tipo(lp.getParams()->at(1));
			string nombre(lp.getParams()->at(0));

			if (tipo == "Hub") {
				this->numHubs++;
			} else if (tipo == "Node") {
				this->numNodes++;
			} else if (tipo == "CM") {
				this->numCM++;
			} else {
				// Amplificador
				this->numAmp++;
			}

			this->elemNames.insert(nombre);
		} else {
			// Comando Connection
			this->numConnectionElements++;
		}

		this->linea++;
	};

}

void FileCruncher::estaDeclaradoValidar(const string nombre) const throw(exception) {
	if (this->elemNames.count(nombre) == 0) {
		throw networkElementNoDeclarado(nombre, this->linea, this->fileName);
	}
}

/**
 * Valida los argumentos pasados a los comandos
 *
 * precondición : lp->getComando() es un comando válido
 */
void FileCruncher::validarArgumentos( const LineParser & lp) const throw(exception) {

	string cmd(lp.getCommando());

	if (lp.getParams()->size() != ELEMENT_OR_CONNECTION_PARAM_SIZE) {
		throw paramsInvalidos(cmd, this->linea, ELEMENT_OR_CONNECTION_PARAM_SIZE,
				lp.getParams()->size(), this->fileName);
	}

	if ( cmd == CMD_NETWORK_ELEMENT ) {
		// NetworkElement
		string tipo(lp.getParams()->at(1));
		string nombre(lp.getParams()->at(0));

		// ToDo validar con un enum
		if ( tipo != "Hub" && tipo != "Node" && tipo != "Amp" &&  tipo != "CM" ) {
			throw tipoNetowrkelementIncorrecto(tipo, this->linea, this->fileName);
		}

		if (this->elemNames.count(nombre) != 0) {
			throw networkElementDuplicado(nombre, this->linea, this->fileName);
		}
	} else {
		// Connection
		string nombre1(lp.getParams()->at(0));
		string nombre2(lp.getParams()->at(1));

		estaDeclaradoValidar(nombre1);
		estaDeclaradoValidar(nombre2);
	}
}

void FileCruncher::validarNetworkName(const LineParser & lp) const {
	string cmd = lp.getCommando();
	if (cmd != CMD_NETWORK_NAME) {
		throw comandoIncorrecto(cmd, CMD_NETWORK_NAME, this->linea,
				this->fileName);
	}
	if (lp.getParams()->size() != NETWORKNAME_NUM_PARAMS) {
		throw paramsInvalidos(cmd, this->linea, NETWORKNAME_NUM_PARAMS,
				lp.getParams()->size(), this->fileName);
	}
}

void FileCruncher::readNetworkName() throw (exception) {

	this->parsingState = 1;

	string line;
	getline(*in, line);

	LineParser lp(line);
	lp.parse();

	validarNetworkName(lp);

	this->networkName = lp.getParams()->at(0);
	this->linea++;
}

string FileCruncher::getNetworkName() const {
	return 	this->networkName;
}

unsigned int FileCruncher::getNumConnections() const {
	return this->numConnectionElements;
}

unsigned int FileCruncher::getNumHubs() const {
	return this->numHubs;
}

unsigned int FileCruncher::getNumNodes() const {
	return this->numNodes;
}

unsigned int FileCruncher::getNumCMs() const {
	return this->numCM;
}

unsigned int FileCruncher::getNumAmps() const {
	return this->numAmp;
}
