#/bin/bash

__PRG=$(basename ${0})
__run="./tp0.exe"


red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
cyan="$(tput setaf 6)"
off="$(tput sgr0)"

function usage() {
    cat <<EOT

   Test execution helper
   
   usage:
       $__PRG [--mem]
    
    mem : valgrind memory leaks detection
    
EOT

}

function showMsg() {
    echo -e "$__PRG: ${cyan}$1${off}"
}

function showErr() {
    showMsg "${red}ERROR:${off} $*"
}

while [[ ! -z $1 ]]; do
    if [[ "$1" == "--mem" ]]; then
       shift
        __run="valgrind --leak-check=yes --track-origins=yes ${__run}"
    else
       showErr "Unknown option $1"
       usage
       exit -1
    fi
    shift
done


for __file in `ls tests`; do
	showMsg "------------------------------------------------------------------> ${__file}"
	${__run} -i tests/${__file}
done
