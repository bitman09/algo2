/*
 * Exceptions.cpp
 *
 *  Created on: Apr 7, 2014
 */

#include "Exceptions.h"


const int BaseTPException::NO_LINE_NUMBER = -1;
const char * BaseTPException::NO_FILENAME = "";

WrongCommandException::WrongCommandException(string cmd, unsigned int line, string filename) {
	this->cmd = cmd;
	this->expected = string("");
	this->filename = filename;
	this->line = line;
	this->whatCStr = NULL;
}

WrongCommandException::WrongCommandException(string cmd, string expected, unsigned int line, string filename) {
	this->cmd = cmd;
	this->expected = expected;
	this->filename = filename;
	this->line = line;
	this->whatCStr = NULL;
}

WrongCommandException::~WrongCommandException() throw () {
}

const char * WrongCommandException::what() const throw () {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "unknown command: '" << this->cmd << "'";

		if (! this->expected.empty() ) {
			oss << " expected '" << this->expected << "'";
		}
		fillWhat(oss);
	}

	return this->whatCStr;
}

WrongNetworkelementTypeException::WrongNetworkelementTypeException(string elemType, unsigned int line, string filename) {
	this->elemType = elemType;
	BaseTPException();
}

WrongNetworkelementTypeException::~WrongNetworkelementTypeException() throw () {
}

const char * WrongNetworkelementTypeException::what() const throw () {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "unknown network type: " << this->elemType;
		fillWhat(oss);
	}
	return whatCStr;
}

DuplicatedNetworkElementException::DuplicatedNetworkElementException(string name) {
	this->name = name;
	this->line = NO_LINE_NUMBER;
	this->filename = NO_FILENAME;
	this->whatCStr = NULL;
}

DuplicatedNetworkElementException::~DuplicatedNetworkElementException() throw () {
}

const char * DuplicatedNetworkElementException::what() const throw () {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "duplicated network element '" << this->name << "'";
		fillWhat(oss);
	}
	return whatCStr;
}


NetworkElementNoDeclarado::NetworkElementNoDeclarado(string nombre) {
	this->name = nombre;
	BaseTPException();
}


NetworkElementNoDeclarado::~NetworkElementNoDeclarado() throw () {
}

const char * NetworkElementNoDeclarado::what() const throw () {
	if (this->whatCStr == NULL) {
		ostringstream oss;
		oss << "undeclared network element '" << this->name << "'";
		fillWhat(oss);
	}
	return whatCStr;
}

UnexpectedCommandException::UnexpectedCommandException(string cmd, unsigned int line, string filename) {
	this->cmd = cmd;
	BaseTPException();
}

UnexpectedCommandException::~UnexpectedCommandException() throw () {
}

const char * UnexpectedCommandException::what() const throw () {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "unexpected command: " << this->cmd;
		fillWhat(oss);
	}
	return whatCStr;
}

InvalidParametersException::InvalidParametersException(string cmd, unsigned int line, unsigned int expected, unsigned int obtained, string filename) {
	this->cmd = cmd;
	this->expected = expected;
	this->obtained = obtained;
	BaseTPException();
}

InvalidParametersException::~InvalidParametersException() throw () {
}

const char * InvalidParametersException::what() const throw () {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "command " << this->cmd << ": expected " << this->expected;
		oss << " parameters but got " << this->obtained;
		fillWhat(oss);
	}
	return whatCStr;
}

MultipleHubsException::MultipleHubsException(unsigned int line, string filename) {
	this->line = line;
	this->filename = filename;
	this->whatCStr = NULL;
}


MultipleHubsException::MultipleHubsException() {
}

MultipleHubsException::~MultipleHubsException() throw() {
}

const char* MultipleHubsException::what() const throw() {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "only one hub allowed, extra hub";
		fillWhat(oss);
	}
	return whatCStr;
}

DuplicatedConnectionException::DuplicatedConnectionException(string father,
		string child) {
	this->father = father;
	this->child = child;
}

const char* DuplicatedConnectionException::what() const throw() {
	if (this->whatCStr == NULL) {
		ostringstream oss;
		oss << "duplicated connection: parent " << this->father;
		oss << ", child " << this->child;
		fillWhat(oss);
	}
	return whatCStr;
}

DuplicatedConnectionException::~DuplicatedConnectionException() throw() {
}


LoopException::LoopException() {
}

const char* LoopException::what() const throw() {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "loop detected";
		fillWhat(oss);
	}
	return whatCStr;
}

LoopException::~LoopException() throw() {
}

NonFullyConnectedNetworkException::NonFullyConnectedNetworkException() {
}

NonFullyConnectedNetworkException::~NonFullyConnectedNetworkException() throw() {
}

const char* NonFullyConnectedNetworkException::what() const throw() {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "not fully connected network";
		fillWhat(oss);
	}
	return whatCStr;
}

NoHubException::NoHubException() {
}

NoHubException::~NoHubException() throw() {
}

const char* NoHubException::what() const throw() {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "No hub declared";
		fillWhat(oss);
	}
	return whatCStr;
}

TwoParentsException::TwoParentsException(string curentParent, string newParent,
		string child) {
	this->curentParent = curentParent;
	this->newParent = newParent;
	this->child = child;
}

TwoParentsException::~TwoParentsException() throw() {
}

const char* TwoParentsException::what() const throw(){
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "two parents not allowed: current parent " << this->curentParent;
		oss << ", new attempted parent " << this->newParent;
		oss << ", child " << this->child;
		fillWhat(oss);
	}
	return whatCStr;
}

HubNoInboundConnectionsException::HubNoInboundConnectionsException() {
}

HubNoInboundConnectionsException::~HubNoInboundConnectionsException() throw() {
}

const char* HubNoInboundConnectionsException::what() const throw() {
	if ( this->whatCStr == NULL) {	ostringstream oss;
		oss << "a Hub must have outbound connections only";

		fillWhat(oss);
	}
	return whatCStr;
}



CableModemNoOutboundConnectionsException::CableModemNoOutboundConnectionsException() {
}

CableModemNoOutboundConnectionsException::~CableModemNoOutboundConnectionsException() throw() {
}

const char* CableModemNoOutboundConnectionsException::what() const throw() {
	if ( this->whatCStr == NULL) {
		ostringstream oss;
		oss << "a Cable Modem can have inbound connections only";

		fillWhat(oss);
	}
	return whatCStr;
}

BaseTPException::BaseTPException() {
	this->line = NO_LINE_NUMBER;
	this->whatCStr = NULL;
	this->filename = NO_FILENAME;
}

const char* BaseTPException::what() const throw() {
	return this->whatCStr;
}

void BaseTPException::setLineNum(unsigned int lineNum) {
	this->line = lineNum;
}

void BaseTPException::setFileName(string fileName) {
	this->filename = fileName;
}

BaseTPException::BaseTPException(const BaseTPException& that) {
	this->filename = that.filename;
	this->line = that.line;
	string str(that.what());
	this->whatCStr = new char[str.length() + 1];
	strcpy(this->whatCStr, str.c_str());
}

BaseTPException::~BaseTPException() throw() {
	if ( this->whatCStr != NULL)
		delete [] whatCStr;
}

void BaseTPException::fillWhat(ostringstream &oss) const {
	if ( this->filename != NO_FILENAME) {
		oss << " at " << this->filename;
		if (this->line != NO_LINE_NUMBER ) {
			oss << ":" << this->line;
		}
	}
	this->whatCStr = new char [oss.str().length() + 1];
	strcpy (whatCStr, oss.str().c_str());
}
