/*
 * NetworkParser.h
 *
 *  Created on: Mar 23, 2014
 */

#ifndef NETWORKPARSER_H_
#define NETWORKPARSER_H_

#include <iostream>
#include <exception>
#include <sstream>
#include <cstring>
#include <set>

#include "LineParser.h"
#include "Exceptions.h"
//#include <NetworkInfo.h>
class NetworkInfo;
#include <Network.h>
#include <NetworkElementType.h>

using namespace std;
/**
 * SPEC
 Formato. Podemos representar un esquema simplificado de una red HFC mediante un archivo de texto
en el cual cada línea contiene una operación a realizar sobre el modelo de la red representado en nuestro
programa. Cada una de estas operaciones contiene su nombre seguido de una cantidad de argumentos que
depende de la operación particular
 * 
 **/
class NetworkParser {
	friend class LineParserTestSuite;
	friend class FileParserTestSuite;
private:
	istream *in;
	//ostream *out;
	string fileName;
	unsigned int line;
	Network *network;

	// Estado de parsing del archivo (ver constantes STATE_*)
	unsigned int parsingState;

	static const int STATE_NO_INICIADO = 0;
	static const int STATE_NETWORKNAME = 1;
	static const int STATE_NETWORKELEMENT = 2;
	static const int STATE_CONNECTION = 3;



	static const int NETWORKNAME_NUM_PARAMS = 1;
	static const int ELEMENT_OR_CONNECTION_PARAM_SIZE = 2;
	static const int REPORT_PARAM_POLL_SIZE = 2;
	static const int REPORT_PARAM_SIZE = 1;

	static const char* CMD_NETWORK_NAME;
	static const char* CMD_CONNECTION;
	static const char* CMD_NETWORK_ELEMENT;
	static const char* NETWORK_ELEMENT_HUB;
	static const char* NETWORK_ELEMENT_NODE;
	static const char* NETWORK_ELEMENT_CM;
	static const char* NETWORK_ELEMENT_AMP;

	static const char* CMD_QUERY;
	static const char* CMD_POLL;
	static const char* CMD_FAULT;
	static const char* CMD_CLEAR;


	NetworkElementType converCommandtToNetElemType(string);
	class LineParser {
	private:
		string line;
		string command;
		vector<string> *params;

		void trimEnd(string &);

	public:
		LineParser(const string );
		LineParser(const LineParser & );
		virtual ~LineParser();
		void parse();
		string getCommand() const;
		vector<string> * getParams() const;
	};

	void readNetworkName() throw(exception);
	void readNetworkElements() throw(exception);
	void validateCommand( const LineParser & ) const throw(exception);
	void validateArguments( const LineParser & ) const throw(exception);
	void validateNetworkName( const LineParser  & ) const;

public:
	NetworkParser();
	NetworkParser(istream &, const string);
	NetworkParser(const NetworkParser & );
	virtual ~NetworkParser();
	NetworkInfo parseTopology() throw (exception);
	bool reportState(string &) throw (exception);
	void setParam(istream &, const string &);
	bool readReport(string &);
	void validateReportArg(const LineParser &) const throw (exception);

};

#include <NetworkInfo.h>

#endif /* NETWORKPARSER_H_ */
