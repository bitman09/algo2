/*
 * LineParser.cpp
 *
 *  Created on: Mar 23, 2014
 */


#include "NetworkParser.h"
#include "LineParser.h"

NetworkParser::LineParser::LineParser(const string line) {
	this->line = line;
	this->params = new vector<string>();
	this->command = "";
}

NetworkParser::LineParser::LineParser(const LineParser & lp) {
	this->line = lp.line;
	this->params = new vector<string>(*lp.params);
	this->command = lp.command;
}

NetworkParser::LineParser::~LineParser() {
	if (this->params != NULL)
		delete this->params;
}

void NetworkParser::LineParser::trimEnd(string &s) {
	string whitespaces (" \t\f\v\n\r");

	size_t found = s.find_last_not_of(whitespaces);
	if (found != string::npos) {
		s.erase(found+1);
	}
}

void NetworkParser::LineParser::parse(){
	string cleanS(this->line);
	trimEnd(cleanS);

	stringstream ss(cleanS);

	ss >> this->command;

	/**
	 * SPEC : ... Asimismo, por tratarse de un archivo de texto,
	 * la cantidad de espacios entre tokens puede ser variable
	 */
	string s;
	while( ! ss.eof() && ss.good() ) {
		ss >> s;
		this->params->push_back(s);
	}

}

vector<string> *NetworkParser::LineParser::getParams() const {
	return this->params;
}

string NetworkParser::LineParser::getCommand() const {
	return this->command;
}


