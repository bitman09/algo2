/*
 * DictTestSuite.cpp
 *
 *  Created on: Jun 12, 2014
 */

#include "DictTestSuite.h"

void DictTestSuite::memTest1()
{
    Dictionary<string, int> d;
}

void DictTestSuite::memTest2()
{
    Dictionary<string, int> *d;
    d = new Dictionary<string, int>();
    delete d;
}

void DictTestSuite::memTest3()
{
    Dictionary<string, int> *d;
    d = new Dictionary<string, int>();
    int val = 3;
    string s("abc");
    d->put( s, val );
    delete d;
}

void DictTestSuite::agregar()
{
    Dictionary<string, int> d;

    TEST_ASSERT(!d.exists("abc"));

    int val = 33;
    string key("abc");
    d.put(key, val);
    TEST_ASSERT(d.exists("abc"));
    TEST_ASSERT_EQUALS(33, d.get("abc"));
    TEST_ASSERT(!d.exists("abcd"));
}

void DictTestSuite::invalidKey()
{
    Dictionary<string, int> d;

    string key("abc");
    TEST_ASSERT(!d.exists(key));
    TEST_THROWS(d.get(key), DictKeyNotFound)
}

void DictTestSuite::opEq()
{
    Dictionary<string, int> d;

    int val = 33;
    string k1("abc");
    d.put(k1, val);

    Dictionary<string, int> d2;
    TEST_ASSERT(!d2.exists(k1));

    d2 = d;
    TEST_ASSERT(&d2 != &d);
    TEST_ASSERT(d2.exists(k1));


    int val2 = 34;
    string k2("efgh");
    d.put(k2, val2);
    TEST_ASSERT(d.exists(k2));
    TEST_ASSERT(!d2.exists(k2));
    TEST_ASSERT_EQUALS(34, d.get(k2));
}

void DictTestSuite::rehash()
{
    Dictionary<int, int> d;
    int limit = 20;

    int key[limit];
    int value[limit];

    /**
     * Se crean de esta forma porque el Diccionario toma la
     * referencia de la clave y el valor, y si uso una sola
     * variable que modifico cambiándole su valor, lo que va
     * a pasar es que todsa las claves y valores van a ser
     * iguales porque son punteros iguales !!
     */
    for (int k = 0, v = 200; k < limit; k++, v--) {
        key[k] = k;
        value[k] = v;
    }

    for (int k = 0; k < limit; k++) {
        ostringstream oss;
        oss << "FAULTING key : " << k;
        TEST_THROWS_NOTHING_MSG(d.put(key[k], value[k]), oss.str().c_str());
    }

    for (int k = 0; k < limit; k++) {
        TEST_ASSERT_EQUALS(value[k], d.get(key[k]) )
    }

    for (int k = 0; k < limit; k++) {
        TEST_ASSERT(d.exists(key[k]) )
    }
}

void DictTestSuite::useString()
{
    Dictionary<string, int> d;
    int limit = 30;

    string key[limit];
    int value[limit];

    /**
     * Se crean de esta forma porque el Diccionario toma la
     * referencia de la clave y el valor, y si uso una sola
     * variable que modifico cambiándole su valor, lo que va
     * a pasar es que todas las claves y valores van a ser
     * iguales porque son punteros iguales !!
     */
    for (int i = 0, v = 200; i < limit; i++, v--) {
        char buffer[20];
        sprintf(buffer, "%u", i);
        string keyS(buffer);

        key[i] = keyS;
        value[i] = v;
    }

    for (int i = 0; i < limit; i++) {
        ostringstream oss;
        oss << "FAULTING key : " << i;
        TEST_THROWS_NOTHING_MSG(d.put(key[i], value[i]), oss.str().c_str());
    }


    for (int k = 0; k < limit; k++) {
        TEST_ASSERT_EQUALS(value[k], d.get(key[k]) )
    }

    for (int k = 0; k < limit; k++) {
        TEST_ASSERT(d.exists(key[k]) )
    }

}

void DictTestSuite::useString2()
{
    Dictionary<string, int> d;
    int limit = 30;

    for (int i = 0; i < limit; i++) {
        ostringstream oss;
        oss << "FAULTING key : " << i;

        ostringstream oss2;
        oss2 << "FAULTING value : " << i;

        char buffer[20];
        sprintf(buffer, "%u", i);
        string *key = new string(buffer);

        int *value = new int(i);

        TEST_THROWS_NOTHING_MSG(d.put(*key, *value), oss.str().c_str())

        TEST_ASSERT_MSG(i == d.get(*key), oss2.str().c_str() )
    }

    for (int k = 0; k < limit; k++) {
        char buffer[20];
        sprintf(buffer, "%u", k);
        string key(buffer);

        ostringstream oss;
        oss << "FAULTING key : " << k;

        TEST_ASSERT_MSG( d.exists(key), oss.str().c_str() )
    }

    for (int k = 0; k < limit; k++) {

         char buffer[20];
         sprintf(buffer, "%u", k);
         string key(buffer);

         ostringstream oss;
         oss << "key : " << k << ",  expected value : " << k << " , obtained value : " <<  d.get(key);

        TEST_ASSERT_MSG(k == d.get(key), oss.str().c_str() )
    }

}


void DictTestSuite::iteratorEmpty()
{

    Dictionary<string, int> d;
    Dictionary<string, int>::Iterator it(d);

    TEST_ASSERT(! it.hasMoreElements() )

}

void DictTestSuite::iterator1()
{
    Dictionary<string, int> d;
    Dictionary<string, int>::Iterator it(d);

    string k("aaaaa");
    int val = 32;
    d.put(k, val);
    TEST_ASSERT_EQUALS(32, d.get(k))

    TEST_ASSERT( it.hasMoreElements() )
    TEST_ASSERT_EQUALS( k, *it.next() );
    TEST_ASSERT( ! it.hasMoreElements() );
}
