/*
 * NetworkElementTestSuite.h
 *
 *  Created on: May 18, 2014
 */

#ifndef NETWORKELEMENTTESTSUITE_H_
#define NETWORKELEMENTTESTSUITE_H_

#include <NetworkElement.h>
#include <NetworkElementType.h>
#include <cpptest.h>

class NetworkElementTestSuite: public Test::Suite {
public:
	NetworkElementTestSuite() {
		TEST_ADD(NetworkElementTestSuite::addChildrenSameName)
		TEST_ADD(NetworkElementTestSuite::addSameChildren)
		TEST_ADD(NetworkElementTestSuite::addChildrenDifferentName)
	}
private:
	void addSameChildren();
	void addChildrenSameName();
	void addChildrenDifferentName();
};

#endif /* NETWORKELEMENTTESTSUITE_H_ */
