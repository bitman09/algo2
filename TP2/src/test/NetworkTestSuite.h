/*
 * NetworkTestSuite.h
 *
 *  Created on: May 3, 2014
 */

#ifndef NETWORKTESTSUITE_H_
#define NETWORKTESTSUITE_H_

#include <cpptest.h>
#include <Network.h>

class NetworkTestSuite: public Test::Suite {
public:
	NetworkTestSuite() {
		TEST_ADD(NetworkTestSuite::noNameProject)
		TEST_ADD(NetworkTestSuite::emptyProject)
		TEST_ADD(NetworkTestSuite::singleHub)
		TEST_ADD(NetworkTestSuite::singleAmp)
		TEST_ADD(NetworkTestSuite::singleCM)
		TEST_ADD(NetworkTestSuite::singleNode)
		TEST_ADD(NetworkTestSuite::duplicatedNetworkElement)
		TEST_ADD(NetworkTestSuite::oneConnection)
		TEST_ADD(NetworkTestSuite::twoConnections)
		TEST_ADD(NetworkTestSuite::duplicatedConnection)
		TEST_ADD(NetworkTestSuite::selfConnection)
		TEST_ADD(NetworkTestSuite::undeclaredChildNetworkElementConnection)
		TEST_ADD(NetworkTestSuite::undeclaredParentNetworkElementConnection)
		TEST_ADD(NetworkTestSuite::twoParentsConnection)
		TEST_ADD(NetworkTestSuite::multipleHubs)
		TEST_ADD(NetworkTestSuite::hubsOutboundConnectionsOnly)
		TEST_ADD(NetworkTestSuite::cmInboundConnectionsOnly)
		TEST_ADD(NetworkTestSuite::verifyNoHub)
		TEST_ADD(NetworkTestSuite::verifyNoFullyConnected)
		TEST_ADD(NetworkTestSuite::verifyNoFullyConnected2)
	}
private:
	void noNameProject();
	void emptyProject();
	void singleHub();
	void singleAmp();
	void singleCM();
	void singleNode();
	void duplicatedNetworkElement();
	void oneConnection();
	void twoConnections();
	void duplicatedConnection();
	void selfConnection();
	void undeclaredChildNetworkElementConnection();
	void undeclaredParentNetworkElementConnection();
	void twoParentsConnection();
	void multipleHubs();
	void hubsOutboundConnectionsOnly();
	void cmInboundConnectionsOnly();
	void verifyNoHub();
	void verifyNoFullyConnected();
	void verifyNoFullyConnected2();
};

#endif /* NETWORKTESTSUITE_H_ */
