/*
 * Addenda.h
 *
 *  Created on: Jun 23, 2014
 */

#ifndef TESTADDENDA_H_
#define TESTADDENDA_H_


#define TEST_THROWS_NOTHING_LOG(expr)                               \
    {                                                               \
        bool __expected = true;                                     \
        char *__msg;                                                \
        try { expr; }                                               \
        catch (exception &e) {                                      \
            __expected = false;                                     \
            __msg = new char[strlen(e.what()) + 1 ];                \
            strcpy(__msg, e.what());                                \
        }                                                           \
        if (!__expected)                                            \
        {                                                           \
            assertment(::Test::Source(__FILE__, __LINE__, __msg));  \
            if (!continue_after_failure()) return;                  \
        }                                                           \
    }


#define TEST_THROWS_WHAT_CONTAINS(expr, x, whatMsg)                                 \
    {                                                                               \
        bool __expectedX = false;                                                   \
        bool __expectedMsg = false;                                                 \
        try { expr; }                                                               \
        catch (x &e)            {                                                   \
            __expectedX = true;                                                     \
            string what(e.what());                                                  \
            if ( what.find(whatMsg) != string::npos ) {                             \
                __expectedMsg = true;                                               \
            }                                                                       \
        }                                                                           \
        catch (...)         {}                                                      \
        if (!__expectedX)                                                           \
        {                                                                           \
            assertment(::Test::Source(__FILE__, __LINE__, "wrong Exception"));      \
            if (!continue_after_failure()) return;                                  \
        }                                                                           \
        if (!__expectedMsg)                                                         \
        {                                                                           \
            assertment(::Test::Source(__FILE__, __LINE__, "wrong what Message"));   \
            if (!continue_after_failure()) return;                                  \
        }                                                                           \
    }


#endif /* TESTADDENDA_H_ */
