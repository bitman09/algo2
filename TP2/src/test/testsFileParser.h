/*
 * testsFileParser.h
 *
 *  Created on: Apr 26, 2014
 */

#ifndef TESTSFILEPARSER_H_
#define TESTSFILEPARSER_H_

#include "NetworkParser.h"
#include "NetworkInfo.h"
#include <string>
#include <cpptest.h>
#include "TestAddenda.h"

class FileParserTestSuite: public Test::Suite {
public:
	FileParserTestSuite() {
		TEST_ADD(FileParserTestSuite::networkName)
		TEST_ADD(FileParserTestSuite::networkNameMal)
		TEST_ADD(FileParserTestSuite::networkNameMalParams)
		TEST_ADD(FileParserTestSuite::networkElement)
		TEST_ADD(FileParserTestSuite::networkElementMal)
		TEST_ADD(FileParserTestSuite::networkElementTipoInvalido)
		TEST_ADD(FileParserTestSuite::networkElement2)
		TEST_ADD(FileParserTestSuite::networkElementCountHubs)
		TEST_ADD(FileParserTestSuite::networkElementCountAmps)
		TEST_ADD(FileParserTestSuite::networkElementCountCMs)
		TEST_ADD(FileParserTestSuite::networkElementCountNodes)
		TEST_ADD(FileParserTestSuite::networkElementEstaDuplicado)
		TEST_ADD(FileParserTestSuite::connection)
		TEST_ADD(FileParserTestSuite::connectionMal)
		TEST_ADD(FileParserTestSuite::connection2)
		TEST_ADD(FileParserTestSuite::connectionMix)
		TEST_ADD(FileParserTestSuite::connectionCmdNoExiste)
		TEST_ADD(FileParserTestSuite::connectionCmdFueraDeOrden)
		TEST_ADD(FileParserTestSuite::connectionNewtorkElementNoDeclarado)
		TEST_ADD(FileParserTestSuite::coonnectionTwoParents)
		TEST_ADD(FileParserTestSuite::connectionMultipleHubs)
		TEST_ADD(FileParserTestSuite::connectionDuplicated)
		TEST_ADD(FileParserTestSuite::hubNoInboundConnection)
		TEST_ADD(FileParserTestSuite::noHub)
	}

private:
	void networkName();
	void networkNameMal();
	void networkNameMalParams();
	void networkElement();
	void networkElementMal();
	void networkElementTipoInvalido();
	void networkElement2();
	void networkElementCountHubs();
	void networkElementCountAmps();
	void networkElementCountCMs();
	void networkElementCountNodes();
	void networkElementEstaDuplicado();
	void connection();
	void connectionMal();
	void connection2();
	void connectionMix();
	void connectionCmdNoExiste();
	void connectionCmdFueraDeOrden();
	void connectionNewtorkElementNoDeclarado();
	void coonnectionTwoParents();
	void connectionMultipleHubs();
	void connectionDuplicated();
	void hubNoInboundConnection();
	void noHub();
};


#endif /* TESTSFILEPARSER_H_ */
