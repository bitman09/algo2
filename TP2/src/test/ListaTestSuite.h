/*
 * ListaTestSuite.h
 *
 *  Created on: May 19, 2014
 */

#ifndef LISTATESTSUITE_H_
#define LISTATESTSUITE_H_

#include <Lista.h>
#include <cpptest.h>

class ListaTestSuite: public Test::Suite {
public:
    ListaTestSuite() {
    	TEST_ADD(ListaTestSuite::nuevaLista)
		TEST_ADD(ListaTestSuite::nuevaListaBorrar)
		TEST_ADD(ListaTestSuite::unElemPrincipio)
		TEST_ADD(ListaTestSuite::unElemPrincipioBorrar)
		TEST_ADD(ListaTestSuite::unElemFinal)
		TEST_ADD(ListaTestSuite::unElemFinalBorrar)
		TEST_ADD(ListaTestSuite::dosElemPrincipio)
		TEST_ADD(ListaTestSuite::dosElemPrincipioBorrar)
		TEST_ADD(ListaTestSuite::dosElemFinal)
		TEST_ADD(ListaTestSuite::dosElemFinalBorrar)
		TEST_ADD(ListaTestSuite::buscarVacia)
		TEST_ADD(ListaTestSuite::buscarLlena)
		TEST_ADD(ListaTestSuite::buscarLlenaYnoHalar)
		TEST_ADD(ListaTestSuite::itEmpty)
		TEST_ADD(ListaTestSuite::itOneElement)
		TEST_ADD(ListaTestSuite::itMoreElements)
    }
private:
	void nuevaLista();
    void nuevaListaBorrar();
    void unElemPrincipio();
    void unElemPrincipioBorrar();
    void unElemFinal();
    void unElemFinalBorrar();
    void dosElemPrincipio();
    void dosElemPrincipioBorrar();
    void dosElemFinal();
    void dosElemFinalBorrar();
    void buscarVacia();
    void buscarLlena();
    void buscarLlenaYnoHalar();
    void itEmpty();
    void itOneElement();
    void itMoreElements();
};


#endif /* LISTATESTSUITE_H_ */
