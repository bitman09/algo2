/*
 * NetworkTestSuite.cpp
 *
 *  Created on: May 3, 2014
 */

#include "NetworkTestSuite.h"

void NetworkTestSuite::noNameProject() {
	Network network;
	TEST_ASSERT_EQUALS("", network.getName());
}

void NetworkTestSuite::emptyProject() {
	Network network;
	TEST_THROWS_NOTHING( network.setName("pepito") );
	TEST_ASSERT_EQUALS("pepito", network.getName());
}

void NetworkTestSuite::singleHub() {
	Network network;
	TEST_THROWS_NOTHING( network.add("Hub1", Hub) );
	TEST_ASSERT_EQUALS(0, network.getNumAmps());
	TEST_ASSERT_EQUALS(0, network.getNumCMs());
	TEST_ASSERT_EQUALS(0, network.getNumConnections());
	TEST_ASSERT_EQUALS(1, network.getNumHubs());
	TEST_ASSERT_EQUALS(0, network.getNumNodes());
}

void NetworkTestSuite::singleAmp() {
	Network network;
	TEST_THROWS_NOTHING( network.add("Amp1", Amp) );
	TEST_ASSERT_EQUALS(1, network.getNumAmps());
	TEST_ASSERT_EQUALS(0, network.getNumCMs());
	TEST_ASSERT_EQUALS(0, network.getNumConnections());
	TEST_ASSERT_EQUALS(0, network.getNumHubs());
	TEST_ASSERT_EQUALS(0, network.getNumNodes());
}

void NetworkTestSuite::singleCM() {
	Network network;
	TEST_THROWS_NOTHING( network.add("CM1", CM) );
	TEST_ASSERT_EQUALS(0, network.getNumAmps());
	TEST_ASSERT_EQUALS(1, network.getNumCMs());
	TEST_ASSERT_EQUALS(0, network.getNumConnections());
	TEST_ASSERT_EQUALS(0, network.getNumHubs());
	TEST_ASSERT_EQUALS(0, network.getNumNodes());
}

void NetworkTestSuite::singleNode() {
	Network network;
	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_ASSERT_EQUALS(0, network.getNumAmps());
	TEST_ASSERT_EQUALS(0, network.getNumCMs());
	TEST_ASSERT_EQUALS(0, network.getNumConnections());
	TEST_ASSERT_EQUALS(0, network.getNumHubs());
	TEST_ASSERT_EQUALS(1, network.getNumNodes());
}

void NetworkTestSuite::oneConnection() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS_NOTHING( network.add("Node2", Node) );
	TEST_ASSERT_EQUALS(0, network.getNumAmps());
	TEST_ASSERT_EQUALS(0, network.getNumCMs());
	TEST_ASSERT_EQUALS(0, network.getNumConnections());
	TEST_ASSERT_EQUALS(0, network.getNumHubs());
	TEST_ASSERT_EQUALS(2, network.getNumNodes());

	TEST_THROWS_NOTHING( network.connect("Node1", "Node2") );
	TEST_ASSERT_EQUALS(1, network.getNumConnections());

}

void NetworkTestSuite::duplicatedNetworkElement() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS(network.add("Node1", Node), DuplicatedNetworkElementException)
}

void NetworkTestSuite::duplicatedConnection() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS_NOTHING( network.add("Node2", Node) );
	TEST_THROWS_NOTHING( network.connect("Node1", "Node2") );
	TEST_THROWS(network.connect("Node1", "Node2"), DuplicatedConnectionException);
}

void NetworkTestSuite::selfConnection() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS(network.connect("Node1", "Node1"), LoopException);
}

void NetworkTestSuite::undeclaredChildNetworkElementConnection() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS(network.connect("Node1", "Node2"), NetworkElementNoDeclarado );
}

void NetworkTestSuite::undeclaredParentNetworkElementConnection() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS(network.connect("Node2", "Node1"), NetworkElementNoDeclarado );
}

void NetworkTestSuite::twoParentsConnection() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS_NOTHING( network.add("Node2", Node) );
	TEST_THROWS_NOTHING( network.add("Node3", Node) );
	TEST_THROWS_NOTHING( network.connect("Node1", "Node2") );
	TEST_THROWS( network.connect("Node3", "Node2"), TwoParentsException);
}

void NetworkTestSuite::twoConnections() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS_NOTHING( network.add("Node2", Node) );
	TEST_THROWS_NOTHING( network.add("Node3", Node) );

	TEST_THROWS_NOTHING( network.connect("Node1", "Node2") );
	TEST_THROWS_NOTHING( network.connect("Node1", "Node3") );
	TEST_ASSERT_EQUALS(2, network.getNumConnections());
}

void NetworkTestSuite::multipleHubs() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Hub1", Hub) );
	TEST_THROWS( network.add("Hub2", Hub), MultipleHubsException );
}

void NetworkTestSuite::hubsOutboundConnectionsOnly() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Hub1", Hub) );
	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS( network.connect("Node1", "Hub1"), HubNoInboundConnectionsException);
}

void NetworkTestSuite::cmInboundConnectionsOnly() {
	Network network;

	TEST_THROWS_NOTHING( network.add("Hub1", Hub) );
	TEST_THROWS_NOTHING( network.add("Node1", Node) );
	TEST_THROWS_NOTHING( network.add("CM1", CM) );
	TEST_THROWS( network.connect("CM1", "Node1"), CableModemNoOutboundConnectionsException);
}

void NetworkTestSuite::verifyNoHub() {
	Network network;
	TEST_THROWS_NOTHING( network.add("Amp1", Amp) );
	TEST_THROWS(network.verify(), NoHubException)
}

void NetworkTestSuite::verifyNoFullyConnected() {
	Network network;
	TEST_THROWS_NOTHING( network.add("Hub1", Hub) );
	TEST_THROWS_NOTHING( network.add("Amp1", Amp) );
	TEST_THROWS(network.verify(), NonFullyConnectedNetworkException)
}

void NetworkTestSuite::verifyNoFullyConnected2() {
	Network network;
	TEST_THROWS_NOTHING( network.add("Hub1", Hub) );
	TEST_THROWS_NOTHING( network.add("Amp1", Amp) );
	TEST_THROWS_NOTHING( network.add("Amp2", Amp) );
	TEST_THROWS_NOTHING( network.add("Amp3", Amp) );
	TEST_THROWS_NOTHING( network.connect("Amp3", "Amp2") );
	TEST_THROWS_NOTHING( network.connect("Amp2", "Amp1") );
	TEST_THROWS_NOTHING( network.connect("Amp1", "Amp3") );
	TEST_THROWS(network.verify(), NonFullyConnectedNetworkException)
}
