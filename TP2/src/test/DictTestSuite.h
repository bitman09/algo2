/*
 * DictTestSuite.h
 *
 *  Created on: Jun 12, 2014
 */

#ifndef DICTTESTSUITE_H_
#define DICTTESTSUITE_H_

using namespace std;

#include <cpptest.h>
#include "Dictionary.h"
#include <string>
#include <cstdio>

class DictTestSuite: public Test::Suite {
public:
	DictTestSuite() {
        TEST_ADD(DictTestSuite::memTest1)
        TEST_ADD(DictTestSuite::memTest2)
        TEST_ADD(DictTestSuite::memTest3)
		TEST_ADD(DictTestSuite::agregar)
		TEST_ADD(DictTestSuite::opEq)
		TEST_ADD(DictTestSuite::invalidKey)
        TEST_ADD(DictTestSuite::rehash)
        TEST_ADD(DictTestSuite::useString)
        TEST_ADD(DictTestSuite::useString2)
        TEST_ADD(DictTestSuite::iteratorEmpty)
        TEST_ADD(DictTestSuite::iterator1)
	}

private:
    void memTest1();
    void memTest2();
    void memTest3();
	void agregar();
	void opEq();
	void invalidKey();
	void rehash();
    void useString();
    void useString2();
	void iteratorEmpty();
	void iterator1();
};


#endif /* DICTTESTSUITE_H_ */
