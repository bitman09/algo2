/*
 * Diagnostics.h
 *
 *  Created on: Jun 24, 2014
 */

#ifndef DIAGNOSTICS_H_
#define DIAGNOSTICS_H_


#define DIAG(x)         if ( verbose ) { cout << this->prefix <<  x << endl; };
#define DIAG_INC(x)     DIAG(x) incDiagLevel();
#define DIAG_DEC()      decDiagLevel();
#define DIAG_I(x)       if ( d->verbose ) { cout << d->prefix << x << endl; };

#endif /* DIAGNOSTICS_H_ */
