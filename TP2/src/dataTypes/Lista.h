/*
 * Lista.h
 *
 *  Created on: May 19, 2014
 */

#ifndef LISTA_H_
#define LISTA_H_

#include <iostream>
using namespace std;

template<typename T>
class LinkedList {
private:
	class Node {
	public:
		T value;
		Node *next;
		Node *prev;
		~Node();
	};

	Node *first;
	Node *last;
	unsigned int numNodes;

public:
	LinkedList();
	~LinkedList();
	bool empty();
	void insert(const T &);
	void add(const T &);
	T *search(const T&);
	unsigned int length();
	bool erase(unsigned int);
	T & operator[](unsigned int);

	class Iterator {
	private:
		Node *current;
	public:
		Iterator(LinkedList &);
		~Iterator();
		bool end() const;
		T elem();
		void operator++(int);
		bool operator!=(const Iterator &);
		bool operator==(const Iterator &);
	};

};

template<typename T>
T & LinkedList<T>::operator[](unsigned int p) {
	if ( p > ( length() - 1 ) ) {
		T *ret = NULL;
		return *ret;
	}

	Node *current = first;
	for (int i = 0; i < p; i++ ) {
		current = current->next;
	}

	return current->value;
}

template<typename T>
bool LinkedList<T>::erase(unsigned int p) {
	if ( (p+1) > length() ) {
		return false;
	}

	Node *current = first;
	for (int i = 0; i < p; i++ ) {
		current = current->next;
	}

	if ( current != first) {
		current->prev->next = current->next;
	} else {
		first = current->next;
	}

	if (current->next == NULL) {
		last = current->prev;
	} else {
		current->next->prev = current->prev;
	}

	delete current;

	numNodes--;

	return true;

}

template<typename T>
LinkedList<T>::LinkedList() {
	first = NULL;
	last = NULL;
	numNodes = 0;
}


template<typename T>
LinkedList<T>::~LinkedList() {
	Node *del = first;
	while( del != NULL) {
		Node *next = del->next;
		delete del;
		del = next;
	}
}


template<typename T>
bool LinkedList<T>::empty() {
	return (numNodes == 0);
}


template<typename T>
void LinkedList<T>::insert(const T &e) {
	Node *n = new Node();
	n->value = e;
	n->next = first;
	n->prev = NULL;

	if ( first != NULL ) { // lista vacía
		first->prev = n;
	}

	first = n;

	if ( last == NULL ) { // lista vacía
		last = n;
	}

	numNodes++;
}


template<typename T>
void LinkedList<T>::add(const T &e) {
	Node *n = new Node();
	n->value = e;
	n->next = NULL;
	n->prev = last;

	if ( last != NULL ) { // lista vacía
		last->next = n;
	}

	last = n;

	if ( first == NULL ) { // lista vacía
		first = n;
	}
	numNodes++;
}


template<typename T>
T *LinkedList<T>::search(const T& e) {
	Node *current;

	current = first;
	bool continueSearch = true;
	while( continueSearch ) {
		if (current == NULL ) {
			continueSearch = false;
		} else {
			continueSearch = (current->value != e);

			if ( continueSearch)
				current = current->next;
		}
	}

	return (current == NULL ? NULL : &(current->value) );
}

template<typename T>
unsigned int LinkedList<T>::length() {
	return numNodes;
}

template<typename T>
inline LinkedList<T>::Node::~Node() {
}

template<typename T>
inline LinkedList<T>::Iterator::Iterator(LinkedList &l) {
	this->current = l.first;
}

template<typename T>
inline LinkedList<T>::Iterator::~Iterator() {
}

template<typename T>
inline bool LinkedList<T>::Iterator::end() const {
	return (this->current == NULL);
}

template<typename T>
inline void LinkedList<T>::Iterator::operator++(int) {
	this->current = this->current->next;
}

template<typename T>
inline bool LinkedList<T>::Iterator::operator!=(const Iterator& it) {
	return( this->current != it.current);
}

template<typename T>
inline T LinkedList<T>::Iterator::elem() {
	return this->current->value;
}

template<typename T>
inline bool LinkedList<T>::Iterator::operator==(const Iterator& it) {
	return( this->current == it.current);
}

#endif /* LISTA_H_ */
