/*
 * DictionaryExceptions.h
 *
 *  Created on: Jun 20, 2014
 */

#ifndef DICTIONARY_EXCEPTIONS_H_
#define DICTIONARY_EXCEPTIONS_H_

#include <exception>
#include <string>
#include <sstream>
#include <cstring>

using namespace std;

class BaseDictException: public exception {
protected:
	mutable char * whatCStr;

	void fillWhat(ostringstream &) const;

public:
	BaseDictException();
    const char * what() const throw ();
    ~BaseDictException() throw();
};

class DictKeyNotFound: public BaseDictException {
private:
	string key;

public:
	DictKeyNotFound(string key);
    const char * what() const throw ();
	~DictKeyNotFound() throw();

};

class DictRehashError: public BaseDictException {
public:
	DictRehashError();
    const char * what() const throw ();
	~DictRehashError() throw();
};


#endif /* DICTIONARY_EXCEPTIONS_H_ */
