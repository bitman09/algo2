/*
 * DictionaryExceptions.cpp
 *
 *  Created on: Jun 20, 2014
 *      Author: vicrodri
 */

#include "DictionaryExceptions.h"

DictKeyNotFound::DictKeyNotFound(string key) {
	this->key = key;
}

const char* DictKeyNotFound::what() const throw() {
	ostringstream oss;
	oss << "Invalid key: '" << this->key << "'";
	fillWhat(oss);
	return this->whatCStr;
}

DictKeyNotFound::~DictKeyNotFound() throw() {
}

DictRehashError::DictRehashError() {
}

const char* DictRehashError::what() const throw() {
	return "Internal rehash error";
}

DictRehashError::~DictRehashError() throw() {
}

void BaseDictException::fillWhat(ostringstream& oss) const {
	this->whatCStr = new char [oss.str().length() + 1];
	strcpy(whatCStr, oss.str().c_str());
}

BaseDictException::BaseDictException() {
}

const char* BaseDictException::what() const throw() {
	return this->whatCStr;
}

BaseDictException::~BaseDictException() throw() {
}
