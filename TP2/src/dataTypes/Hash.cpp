

#include "Hash.h"


static int MAGIC_PRIME_1 = (unsigned int) 63689;
static int MAGIC_PRIME_2 = (unsigned int) 378551;

using namespace std;

unsigned int hash( unsigned int colision, int v ) {
	    unsigned int myHash =  abs(v) + colision ;
		return myHash;
}

unsigned int hash( string v, unsigned int begin, unsigned int thisIsTheEnd ) {
	unsigned int hash = 0;
	unsigned int a = MAGIC_PRIME_1;

	for( unsigned int i=begin; i <= thisIsTheEnd; i++ ) {
        hash = (unsigned int) ( a * hash + v[i] );
        a *= MAGIC_PRIME_2;
	};
    
 	return hash;
}

unsigned int hash( unsigned int collision,  string key) {

    unsigned int middle = floor( key.length() / 2 );

    /**
     * Se elige que la primer parte del hash sea la que ajuste según las colisiones
     * porque en el caso de strings muy chicos (longitud 1 o 2) la segunda parte vale
     * cero, por lo tanto siempre da el mismo has para todas las colisiones y queda
     * indefinidamente buscando un nuevo bucket libre
     */
	unsigned int myHash = ( collision * hash( key, 0, middle ) +
    	     			    ( hash( key, middle + 1, key.length() - 1 ) )
    	   				   );
	return myHash;
}

