
#ifndef _DICTIONARY_H
#define _DICTIONARY_H

using namespace std;

#include <cassert>
#include <iostream>
#include <stdlib.h>

#include "Hash.h"
#include "DictionaryExceptions.h"
#include "Diagnostics.h"

template<class K, class V> class Dictionary {
            friend class Iterator;
public:
	Dictionary();
	~Dictionary();
	Dictionary(const Dictionary<K, V>& d);
	Dictionary<K, V>& operator=(const Dictionary<K, V>& d);

	void put( K& key, V& value);
	bool exists(const K& key);
	V &get(const K& key) throw(exception);
	void clear();

	// Diagnosis
    void setVerbose();
    void unsetVerbose();

	class Iterator {
	    public:
	        Iterator(Dictionary<K,V> &);
	        K *next();
	        bool hasMoreElements();

	    private:
	        Dictionary<K,V> *d;
	        int currBucket;
	        unsigned int findNextBucket(unsigned int);
	};



private:

	#define BUCKET_SIZE_INIT   20
	#define REHASH_THRESHOLD   (float) 0.7


    struct element {
        K *key;
        V *value;
        bool free;
    };

    struct index {
        bool found;
        unsigned int index;
    };
    
    element *bucket;
    unsigned int bucketSize;
    unsigned int nElems;

    unsigned long int _hash( const K &, unsigned int, unsigned int ) const;
    
    element * createBucket(unsigned int n);
    void byeByeLife();
    void copyBucket( const Dictionary<K, V> & from, Dictionary<K, V> & to );
    index searchKey(const K& key);
    index searchKey(const K& key, element *, unsigned int);
    void rehash() throw(DictRehashError);
    void init(const Dictionary<K,V> &);

    // Diagnosis
    bool verbose;
    unsigned int level;
    string prefix;

    void incDiagLevel();
    void decDiagLevel();
};



template <class K, class V>
typename Dictionary<K, V>::element * Dictionary<K, V>::createBucket(unsigned int n) {
    unsigned int i;

    typename Dictionary<K, V>::element e;
    e.free = true;
    e.value = NULL;
    e.key = NULL;

    typename Dictionary<K, V>::element *thisBucket = new typename Dictionary<K, V>::element[n];

    for ( i=0; i < n; i++) {
        thisBucket[i] = e;
    };
    
    return thisBucket;
}

template <class K, class V>
void Dictionary<K, V>::byeByeLife()
{
        if (bucket != NULL)
            delete[] bucket;

}

template <class K, class V>
void Dictionary<K, V>::copyBucket( const Dictionary<K, V> & from, Dictionary<K, V> & to ) {
	unsigned int i;

	for (i = 0; i < from.bucketSize; i++ ) {
		to.bucket[i] = from.bucket[i];
	};
}

template<class K, class V>
    inline void Dictionary<K, V>::setVerbose()
    {
        verbose = true;
    }

template<class K, class V>
    inline void Dictionary<K, V>::unsetVerbose()
    {
        verbose = false;
    }

template <class K, class V>
void Dictionary<K, V>::rehash() throw(DictRehashError) {
    int newBucketSize = bucketSize * 2 + 1;
	element  *newBucket = createBucket( newBucketSize );
	
	for (unsigned int i = 0; i < bucketSize; i++ ) {
	    DIAG( "rehash() bucket : " << i )
		if (! bucket[i].free) {
            DIAG( "rehash() IS NOT FREE" )
            DIAG_INC( "rehash() key (void *) " << bucket[i].key )
		    K &k = *(bucket[i].key);
			index idx = searchKey(k, newBucket, newBucketSize);
			DIAG_DEC()

			if ( idx.found ) {
			    DIAG( "rehash() DictRehashError (found and shouldn't) key : " << k )
				throw DictRehashError();
			}

			unsigned int dest = idx.index;
			newBucket[dest].value = bucket[i].value;
			newBucket[dest].key = bucket[i].key;
			newBucket[dest].free = false;
		} else {
            DIAG( "rehash() IS FREE" )
		}
	};

	bucketSize = newBucketSize;
	delete [] bucket;
	bucket = newBucket;
}

template <class K, class V>
Dictionary<K, V>::Dictionary()
{
    bucket = createBucket(BUCKET_SIZE_INIT);
    bucketSize = BUCKET_SIZE_INIT;
    nElems = 0;
    verbose = false;
    level = 0;
}

template <class K, class V>
Dictionary<K, V>::~Dictionary()
{
	byeByeLife();
}

template<class K, class V>
    Dictionary<K, V>::Dictionary( const Dictionary<K, V>& d )
    {
        init(d);
    }

template <class K, class V>
unsigned long int Dictionary<K, V>::_hash( const K &key, unsigned int collision, unsigned int localBucketSize ) const {
        unsigned long int myHash = hash(collision, key);

        DIAG(  "__hash(): key : " << key << ", hash : " << myHash << ", collision : " << collision )
        return (myHash % localBucketSize);
}

template <class K, class V>
Dictionary<K, V>& Dictionary<K, V>::operator=( const Dictionary<K, V>& d )
{
	if (this != &d) {
		init(d);
	}
	return *this;
}

template<class K, class V>
    void Dictionary<K, V>::put( K& key, V& value )
    {
        DIAG("put() BEGIN key : " << key << ", value : " << value)

        if (((float) nElems / bucketSize) > REHASH_THRESHOLD) {
            DIAG_INC("put() going to rehash() key : " << key )
            rehash();
            DIAG_DEC()
            DIAG("put() rehash() ended key : " << key )
        }

        DIAG_INC("put() going to search")
        index current = searchKey(key);
        DIAG_DEC()

        if (current.found) {
            DIAG("put() key already in dict, key : " << key)
            bucket[current.index].value = &value;
            return;
        };

        unsigned int i = current.index;

        bucket[i].value = &value;
        bucket[i].key = &key;
        bucket[i].free = false;
        nElems++;

        DIAG("put() END bucket  : " << i )
        DIAG("put() END key  : " << *(bucket[i].key) << ", value : " << *(bucket[i].value) )
        DIAG("put() END key (void *) : " << bucket[i].key << ", value (void *) : " << bucket[i].value )
    }

template <class K, class V>
typename Dictionary<K, V>::index Dictionary<K, V>::searchKey(const K& key)
{
	return searchKey(key, bucket, bucketSize);
}

template <class K, class V>
typename Dictionary<K, V>::index Dictionary<K, V>::searchKey(const K& key, element *localBucket, unsigned int localBucketSize)
{
	DIAG( "searchKey() BEGIN: key : " << key )
	typename Dictionary<K, V>::index ret;
	unsigned int collision = 1;
	bool stop = false;

	while ( ! stop ) {

	    DIAG_INC("searchKey() call _hash() collision : " << collision)
	    unsigned long int i = _hash(key,collision, localBucketSize);
	     //   cout << "i : " << i <<  endl;
	    DIAG_DEC()

        if ( localBucket[i].free) {
			stop = true;
			ret.found = false;
			ret.index = i;

			DIAG( "searchKey() key : " << key << ", bucket : " << i << ", IS FREE" )
		} else if ( *(localBucket[i].key) == key) {
			stop = true;
			ret.found = true;
			ret.index = i;

			DIAG( "searchKey() key : " << key << ", bucket : " << i << ", value : " << *(localBucket[i].value) << ", FOUND IT" )

		} else {
			collision++;
            DIAG( "searchKey() key : " << *(localBucket[i].key) << ", bucket : " << i << ", value : " << *(localBucket[i].value) << ", COLLISION" )
			DIAG( "searchKey() key (void *) " << localBucket[i].key)
		}
	};
	

    DIAG( "searchKey() END" )
	return ret;
}

template <class K, class V>
bool Dictionary<K, V>::exists(const K& key)
{
    DIAG_INC( "exists() key : " << key );
    bool ret = searchKey(key).found;
    DIAG_DEC()
    DIAG( "exists() END key '" << key << "' " << (ret ? "exists" : "doesn't exists"));
	return ret;
}

template <class K, class V>
V &Dictionary<K, V>::get(const K& key) throw(exception)
{

    DIAG_INC("get() BEGIN key : " << key )
	index idx = searchKey(key);
    DIAG_DEC()

	if (! idx.found) {
	    ostringstream oss;
	    oss << key;

	    DIAG( "get() DictKeyNotFound  key : " << key )

		throw DictKeyNotFound(oss.str());
	}

    DIAG("get() END key : " << key << ", value : " << *(bucket[idx.index].value) << ", value (void *) : " << bucket[idx.index].value )

	return *(bucket[idx.index].value);
    }

template<class K, class V>
    Dictionary<K, V>::Iterator::Iterator( Dictionary<K, V> & d )
    {
        this->d = &d;
        this->currBucket = -1;
    }

template<class K, class V>
    inline K *Dictionary<K, V>::Iterator::next()
    {
        currBucket = findNextBucket(currBucket + 1);
        return this->d->bucket[currBucket].key;
    }

template<class K, class V>
    bool Dictionary<K, V>::Iterator::hasMoreElements()
    {
        DIAG_I( "hasMoreElements()" )

        bool has = findNextBucket(currBucket + 1) != -1;

        DIAG_I( "hasMoreElements(): " << (has ? "HAS" : "HAS NOT") )

        return has;
    }

template<class K, class V>
    unsigned int Dictionary<K, V>::Iterator::findNextBucket( unsigned int startingFrom )
    {
        DIAG_I("findNextbucket(): start : " << startingFrom << ", bucketSize : " << d->bucketSize)
        for (int i = startingFrom; i < d->bucketSize; i++) {
            if (!d->bucket[i].free) {
                DIAG_I("findNextbucket() bucket : " << i << ", key : " << *((d->bucket[i]).key))
                return i;
            }
            else {
                DIAG_I("findNextbucket() bucket : " << i << " FREE")
            }
        }
        return -1;

    }

template<class K, class V>
    void Dictionary<K, V>::init(const Dictionary<K,V> &d)
    {
        byeByeLife();
        bucket = createBucket(d.bucketSize);
        bucketSize = d.bucketSize;
        copyBucket(  d, *this );
        nElems = d.nElems;
        verbose = d.verbose;
    }

template<class K, class V>
    void Dictionary<K, V>::incDiagLevel()
    {
        if (verbose) {
            level++;
            prefix = string(this->level, ' ');
        }
    }

template<class K, class V>
    void Dictionary<K, V>::clear()
    {
        unsigned int i;
        for (i = 0; i < bucketSize; i++) {
            if (bucket[i].key != NULL) {
                DIAG("byeByeLife() key : " << *(bucket[i].key))
                delete bucket[i].key;
            }
            if (bucket[i].value != NULL) {
                DIAG("byeByeLife() value : " << *(bucket[i].value) )
                delete bucket[i].value;
            }
        };
    }

template<class K, class V>
    void Dictionary<K, V>::decDiagLevel()
    {
        if (verbose) {
            level--;
            prefix = string(this->level, ' ');
        }
    }

#endif /*_DICTIONARY_H*/

