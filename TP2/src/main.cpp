#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cstdlib>

#include "cmdline.h"
#include "NetworkParser.h"
#include "NetworkInfo.h"
#include "psr/Memory.h"
#include "psr/Timer.h"


using namespace std;


////////////////////////////////////////////////////////////////////////////
//FUNCTION PROTOTYPES

static void opt_input(string const &);
static void opt_input_cmd(string const &);
static void opt_output(string const &);
static void opt_output_cmd(string const &);
static void opt_help(string const &);
static void opt_threshold(string const &);
static void opt_psr(string const &);

////////////////////////////////////////////////////////////////////////////
//GLOBAL VARIABLES




static option_t options[] = {
	{1, "", "topology", "-", opt_input, OPT_DEFAULT},
    {1, "o", "output", "-", opt_output_cmd, OPT_DEFAULT},
    {1, "", "print-topology", NULL, opt_output, OPT_DEFAULT},
    {1, "i", "input", "-", opt_input_cmd, OPT_DEFAULT},
    {1, "t", "", "5", opt_threshold, OPT_DEFAULT},
    {1, "p", "psr", "5", opt_psr, OPT_DEFAULT},
	{0, "h", "help", NULL, opt_help, OPT_DEFAULT},
	{0, },//Indica el final del arreglo
};
static istream *isTopology = 0; // puntero a: cin o al archivo de entrada
static istream *isCommands = 0; // puntero a: cin o al archivo de entrada
static string isName;    // Nombre del archivo de entrada
static string isCmdName;
static ostream *osOutputTopology = 0; // puntero a: cout o al archivo de salida
static ostream *osOutputCmd = 0; // puntero a: cout o al archivo de salida
static ostream *osPSR = 0; // puntero a: cout o al archivo de salida
static fstream ifsTopology; 
static fstream ifsCommands;
static fstream ofsOutputTopology;
static fstream ofsOutputCmd;
static fstream ofsPSR;
static int threshold;

static bool psr = false;  // Mostra estadísticas de performance


////////////////////////////////////////////////////////////////////////////


int main(int argc, char *argv[])
{

	cmdline cmdl(options); //creo el objeto cmdl
	cmdl.parse(argc, argv); // parse de los argumentos de línea de comando


	try {
	    Timer tmr;
	    Memory mem;
	    long memResident;
	    long memVirtual;

        if ( psr ) {
            tmr.start();
            memResident = mem.getResident();
            memVirtual = mem.getVM();
        }

	/**
	 * SPEC : El programa deberá barrer el archivo de topología una única vez, 
	 * y construir el modelo de la red en memoria detectando, y 
	 * diagnosticando los errores (loops, elementos repetidos o inexistentes,
	 * segmentos inconexos, etc).
	 * A continuación, deberá imprimir por salida estándar la descripción
	 * de la topología de la red, generada a partir del modelo.
	 **/

		NetworkParser p(*isTopology, isName);
		NetworkInfo info = p.parseTopology();
		if(osOutputTopology!= NULL)
			info.print(*osOutputTopology);

		info.reportState(* isCommands,*osOutputCmd, threshold,isCmdName);


        if ( psr ) {
            tmr.stop();
            *osPSR << "Elapsed time (msec) : " << tmr.getDeltaMillis() << endl;
            *osPSR << "Resident memory : " <<  mem.getResident() - memResident << endl;
            *osPSR << "Virtual memory : " <<  mem.getVM() - memVirtual << endl;
        }

	}
	catch(exception &e) {
			cerr << "error: " << e.what() << endl;
			return 1;
	}


	return 0;
}



////////////////////////////////////////////////////////////////////////////

static void opt_input(string const &arg)
{
    // Si el nombre del archivos es "-", usaremos la entrada
    // estándar. De lo contrario, abrimos un archivo en modo
    // de lectura.
    //
    if (arg == "-") {
        isTopology = &cin;
    } else {
        ifsTopology.open(arg.c_str(), ios::in);
        isTopology = &ifsTopology;
    }
    isName = (arg == "-") ? "stdin" : arg;

    // Verificamos que el stream este OK.
    //
    if (!isTopology->good()) {
        cerr << "cannot open "
             << arg
             << "."
             << endl;
        exit(1);
    }
}

static void opt_input_cmd(string const &arg)
{
    // Si el nombre del archivos es "-", usaremos la entrada
    // estándar. De lo contrario, abrimos un archivo en modo
    // de lectura.
    //
    if (arg == "-") {
        isCommands = &cin;
    } else {
        ifsCommands.open(arg.c_str(), ios::in);
        isCommands = &ifsCommands;
    }
    isCmdName = (arg == "-") ? "stdin" : arg;

    // Verificamos que el stream este OK.
    //
    if (!isCommands->good()) {
        cerr << "cannot open "
             << arg
             << "."
             << endl;
        exit(1);
    }
}
static void opt_threshold(string const &arg)
{
    // Si el nombre del archivos es "-", usaremos la entrada
    // estándar. De lo contrario, abrimos un archivo en modo
    // de lectura.
    //
    if (arg == "5") {
        threshold = 5;
        return;
    } else{
        threshold = atoi(arg.c_str());
    }
    if(!threshold){
        cerr << "cannot open "
                 << arg
                 << "."
                 << endl;
        exit(1);
    }
}

static void opt_psr(string const &arg)
{
    // Si el nombre del archivos es "-", usaremos la salida
    // estándar. De lo contrario, abrimos un archivo en modo
    // de escritura.
    //
    if (arg == "-") {
        osPSR = &cout;
    } else {
        ofsPSR.open(arg.c_str(), ios::out);
        osPSR = &ofsPSR;
    }

    // Verificamos que el stream este OK.
    //
    if (!osPSR->good()) {
        cerr << "cannot open "
             << arg
             << "."
             << endl;
        exit(1);
    }

    psr = true;
}

static void opt_output_cmd(string const &arg)
{
    // Si el nombre del archivos es "-", usaremos la salida
    // estándar. De lo contrario, abrimos un archivo en modo
    // de escritura.
    //
    if (arg == "-") {
        osOutputCmd = &cout;
    } else {
        ofsOutputCmd.open(arg.c_str(), ios::out);
        osOutputCmd = &ofsOutputCmd;
    }

    // Verificamos que el stream este OK.
    //
    if (!osOutputCmd->good()) {
        cerr << "cannot open "
             << arg
             << "."
             << endl;
        exit(1);
    }
}

static void opt_output(string const &arg)
{
	// Si el nombre del archivos es "-", usaremos la salida
	// estándar. De lo contrario, abrimos un archivo en modo
	// de escritura.
	//
	if (arg == "-") {
		osOutputTopology = &cout;
	} else {
		ofsOutputTopology.open(arg.c_str(), ios::out);
		osOutputTopology = &ofsOutputTopology;
	}

	// Verificamos que el stream este OK.
	//
	if (!osOutputTopology->good()) {
		cerr << "cannot open "
		     << arg
		     << "."
		     << endl;
		exit(1);
	}
}

static void opt_help(string const &arg)
{
	cout << "tp0.exe [-i file] [-o file]"
	     << endl;
	exit(0);
}

////////////////////////////////////////////////////////////////////////////



