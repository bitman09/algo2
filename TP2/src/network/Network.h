/*
 * Network.h
 *
 *  Created on: May 1, 2014
 */

#ifndef NETWORK_H_
#define NETWORK_H_

using namespace std;

#include <string>
#include <iostream>
#include "NetworkElement.h"
#include "Exceptions.h"
#include "Lista.h"
#include "Dictionary.h"
#include "Diagnostics.h"

class Network {
private:

	static const char* NETWORK_NAME;
	static const char* CONNECTION;
	static const char* NETWORK_ELEMENT;

	Dictionary<string, NetworkElement> networkElemContainer;
	
	NetworkElement* root;
	string name;
	unsigned int numConnectionElements;
	unsigned int numHubs;
	unsigned int numNodes;
	unsigned int numCM;
	unsigned int numAmp;
	
	int threshold;

	bool isNetworkElementDeclared( string );
	unsigned int recorrer (NetworkElement &);
	bool CheckStatus(NetworkElement&)const;

	// Diagnostics
    void setVerbose();
    void unsetVerbose();
    bool verbose;
    string prefix;

public:
	Network();
	virtual ~Network();
	void setName(const string);
	void add(string, NetworkElementType) throw(exception);
	void connect(string, string) throw(exception);
	string getName() const;
	unsigned int getNumConnections() const;
	unsigned int getNumHubs() const;
	unsigned int getNumNodes() const;
	unsigned int getNumCMs() const;
	unsigned int getNumAmps() const;
	void verify();
	void outGeneratedInput(ostream&);
	void setReportedFailure(string); //falla manual
	void setAllegedFailure(string); // falla inferida
	bool removeAllegedFailure(string); //quita falla inferida
	bool clear(string); // quita todas las fallas
	bool query(string); //status de elemento
	void setThreshold(int);
	bool isNode(string );
};

#endif /* NETWORK_H_ */
