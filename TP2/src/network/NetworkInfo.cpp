/*
 * NetworkInfo.cpp
 *
 *  Created on: Apr 29, 2014
 */

#include "NetworkInfo.h"
#include "../dataTypes/Diagnostics.h"

NetworkInfo::~NetworkInfo() {
}

NetworkInfo::NetworkInfo(const NetworkInfo& that) {
	if (this != &that) {
		this->network = that.network;
		this->verbose = that.verbose;
		this->prefix = that.prefix;
	}
}

NetworkInfo::NetworkInfo( Network * network, NetworkParser * p )
{
    this->network = network;
    this->report = p;
    this->verbose = (network->getName() == "__DEBUG");
}

void NetworkInfo::print(ostream& oss) const {
	this->network->outGeneratedInput(oss);
}

void NetworkInfo::reportState(istream & in, ostream & out, int & t, string & file) throw (exception) {
    DIAG("NetworkInfo::reportState() BEGIN")
	//cambio el stream de entrada, el nombre del archivo
	report->setParam(in,file);//coloco el nuevo archivo
	string name;
	network->setThreshold(t);

    while (!in.eof()) {
        if (report->reportState(name)) {
            if (name != "" && !in.eof()) {
                out << "NetworkElement " << name << " " << "status Ok" << endl;
            }
        }
        else {
            if (name != "" && !in.eof()) {
                out << "NetworkElement " << name << " " << "status Fault" << endl;
            }
        }

    }

    DIAG("NetworkInfo::reportState() END")
}
