/*
 * Network.cpp
 *
 *  Created on: May 1, 2014
 */

#include "Network.h"

using namespace std;

const char* Network::NETWORK_NAME = "NetworkName";
const char* Network::CONNECTION = "Connection";
const char* Network::NETWORK_ELEMENT = "NetworkElement";

Network::Network() {
	this->name = "";
	this->numAmp = 0;
	this->numCM = 0;
	this->numConnectionElements = 0;
	this->numHubs = 0;
	this->numNodes = 0;
	this->root = NULL;
	this->verbose = false;
}

Network::~Network() {
	/**
	 * Sería mucho más interesante, y una implementación mucho
	 * más elegante, que cada NetworkElement (en el destructor)
	 * libere la memoria de sus hijos, y así sucesivamente en
	 * forma recursiva. De esta forma liberando la memoria del Hub
	 * que está como root del árbol desencadena el borrado del
	 * árbol completo. El problema es que puede ser que la estructura
	 * formada no sea un árbol, por lo tanto para asegurarnos que
	 * se libere toda la memoria se borran cada uno de los NetworkElement
	 * registrados y no se implementa el borrado de los hijos en
	 * el destructor de NetworkElement.
	 */

    networkElemContainer.clear();
}

bool Network::isNode( string str){
	return isNetworkElementDeclared (str);
}

bool Network::isNetworkElementDeclared(string str) {

	bool e = this->networkElemContainer.exists(str);
    return e;
}

void Network::add(string name, NetworkElementType elemType) throw(exception) {

    DIAG("Network::add() BEGIN name : " << name );
	if ( isNetworkElementDeclared(name) ) {
		throw DuplicatedNetworkElementException(name);
	}
	
	NetworkElement *node = new NetworkElement(name,elemType);
	string *nam = new string(name);
	networkElemContainer.put(*nam,*node);

	switch (elemType) {
		/**
		 *  SPEC A lo largo de este trabajo adoptaremos un modelo 
		 * minimalista para representar redes HFC en nuestros sistemas, 
		 * consistiendo de un único hub en la raíz
		 **/
		case Hub : 	if( this->numHubs == 0 ) {
					this->numHubs++;
					this->root = node;
				} else {
				  DIAG("Network::add   MultipleHubsException")
				  throw MultipleHubsException();
				}
				break;

		case Node :	this->numNodes++;
					break;

		case CM: 	this->numCM++;
					break;

		case Amp: 	this->numAmp++;
					break;
	}
    DIAG( "Network::add() end name : " << name );
}

void Network::connect(string parentName, string childName) throw(exception) {

    DIAG( "Network::connect() BEGIN parent : " << parentName << ", child : " << childName );

	if ( ! isNetworkElementDeclared(parentName) ) {
	    DIAG( "Network::connect() NetworkElementNoDeclarado")
		throw NetworkElementNoDeclarado(parentName);
	}

	if ( ! isNetworkElementDeclared(childName) )  {
        DIAG( "Network::connect() NetworkElementNoDeclarado")
		throw NetworkElementNoDeclarado(childName);
	}

	this->numConnectionElements++;
	

    DIAG( "Network::connect() parent and child exist" )
	NetworkElement &parent = this->networkElemContainer.get(parentName);
    DIAG("Network::connect() parent : " << parent << ", parent (void *) : " << &parent )
	NetworkElement &child = this->networkElemContainer.get(childName);
    DIAG("Network::connect() child : " << child << ", child (void *) : " << &child )

	if ( parentName == childName) {
	    DIAG("Network::connect() LoopException")
		throw LoopException();
	} else if ( ! parent.addChildren(child) ) {
        DIAG("Network::connect() DuplicatedConnectionException")
		throw DuplicatedConnectionException(parent.name, child.name);
	} else if ( child.parent != NULL ) {
        DIAG("Network::connect() TwoParentsException")
		throw TwoParentsException(child.parent->name, parentName, childName);
	} else if( child.getType() ==  Hub ) {
        DIAG("Network::connect() HubNoInboundConnectionsException")
		throw HubNoInboundConnectionsException();
	} else if( parent.getType() == CM ) {
	/**
	 * SPEC
	 * 1.- "los equipos alojados en el ámbito de los clientes en las hojas (cable modem)."
	 * 2.- "A lo largo de este trabajo adoptaremos un modelo minimalista para 
	 *      representar redes HFC en nuestros sistemas, consistiendo  ... y dispositivos
	 *      ubicados en el ámbito de los clientes (cable modems)."
	 **/
        DIAG("Network::connect() CableModemNoOutboundConnectionsException")
		throw CableModemNoOutboundConnectionsException();
	} else {
        DIAG("Network::connect() connect child and parent")
		child.parent = &parent;
	}
	DIAG("Network::connect() END parent : " << parentName << ", child : " << childName)

}

string Network::getName() const {
	return this->name;
}

void Network::verify() {
	unsigned int total;	

    DIAG("Network::verify() BEGIN")
	if(numHubs == 0)
		throw NoHubException();
	else
		total = this->recorrer(*root);
	
	if (total != numNodes+numCM+numAmp+numHubs)
		throw NonFullyConnectedNetworkException();

    DIAG("Network::verify() END")
}


unsigned int Network::recorrer (NetworkElement &aux){
	

    DIAG("Network::recorrer() BEGIN")
	unsigned int sumNodos=0;
	if(aux.visit==true)
		/**
		 * Los loops se detectan por construcción si quiero asignar a
		 * un padre a un nodo que ya tiene padre. Igualmente podría generar
		 * un loop pero en este, para que sea visitado por este algoritmo
		 * que empieza  recorrer desde el root, este debería ser hijo de
		 * alguien lo cual está prohibido por construcción (se verifica al
		 * conectar 2 nodos que un Hub no pueda ser hijo)
		 * Esta verificación, por lo tanto, es superflua en el contexto
		 * de este TP y sólo por la forma de construcción de la red,
		 * pero igualmente este código no fue quitado sólo por generalidad
		 * de la solución, y para que pueda ser usado fuera del contexto
		 * de este TP.
		 */
		throw LoopException();
	else{
		aux.visit=true;
	}
	
	string * clave;
	for(Dictionary<string, NetworkElement>::Iterator it(aux.children);  it.hasMoreElements() ; ) {
		clave=	it.next();
		NetworkElement &ne = aux.children.get(*clave);
		sumNodos += this->recorrer( ne );
	}

    DIAG("Network::recorrer() END")
	return sumNodos +1;
}





void Network::setName(const string name) {
	this->name = name;

	if ( name == "__DEBUG" ) {
	    networkElemContainer.setVerbose();
	    setVerbose();
	}
}

unsigned int Network::getNumConnections() const {
	return this->numConnectionElements;
}

unsigned int Network::getNumHubs() const {
	return this->numHubs;
}

unsigned int Network::getNumNodes() const {
	return this->numNodes;
}

unsigned int Network::getNumCMs() const {
	return this->numCM;
}

unsigned int Network::getNumAmps() const {
	return this->numAmp;
}


void Network::outGeneratedInput(ostream& oss) {

	oss << NETWORK_NAME << " " << this->getName() << endl;

	string * key;
	for(Dictionary<string, NetworkElement>::Iterator it(this->networkElemContainer);it.hasMoreElements(); ){
			key=it.next();
			NetworkElement &aux=this->networkElemContainer.get(*key);
			oss << NETWORK_ELEMENT << " " <<  aux.name << " " << aux.getStringType() << endl;
	}

	for(Dictionary<string, NetworkElement>::Iterator it(this->networkElemContainer); it.hasMoreElements();){
			key = it.next();
			NetworkElement &aux   = this->networkElemContainer.get(*key);
			if( aux.getType()  != Hub ) { // HUB no tiene padre
				oss  << CONNECTION << " " << aux.name << " " << aux.parent->name << endl;
			}
	}
}


void Network::setReportedFailure(string str){
	

	NetworkElement &nodo = networkElemContainer.get(str);
 
	if(nodo.ReportedFailure ) return;
	
	nodo.ReportedFailure= true;
	if(!nodo.AllegedFailure && nodo.parent)//para no sumar 2 veces
		nodo.parent->numFailureChildren++;
}


void Network::setAllegedFailure(string str){

	
	NetworkElement &nodo = networkElemContainer.get(str);

	if(nodo.AllegedFailure )
	    return;
	nodo.AllegedFailure= true;
	
	if(! nodo.ReportedFailure && nodo.parent)
		nodo.parent->numFailureChildren++;//para no sumar dos veces la falla
}
	
bool Network::removeAllegedFailure(string str){

	NetworkElement &nodo = networkElemContainer.get(str);

	if(nodo.AllegedFailure){
		nodo.AllegedFailure = false;
		if(!nodo.ReportedFailure  && nodo.parent)
			nodo.parent->numFailureChildren--;
		if(!nodo.parent->FailureChildrenExceed(threshold))
			nodo.parent->AllegedFailure =false;
	}
	if(nodo.ReportedFailure)
			return false;
	if(nodo.parent)
		return CheckStatus(*(nodo.parent));
	return true;
}

bool Network::clear(string str){

	NetworkElement &nodo = networkElemContainer.get(str);

	if(nodo.AllegedFailure || nodo.ReportedFailure){

	nodo.AllegedFailure = false;
	nodo.ReportedFailure = false;
	if(nodo.parent)
		(nodo.parent->numFailureChildren)--;
	if(!nodo.parent->FailureChildrenExceed(threshold))
			nodo.AllegedFailure =false;
	}
	if (nodo.parent)
		return CheckStatus(*(nodo.parent));
	return true;
}			

void Network::setVerbose()
{
    verbose = true;
}

void Network::unsetVerbose()
{

    verbose = false;
}

bool Network::query(string str){  //true = ok False =error

	NetworkElement &nodo = networkElemContainer.get(str);
	return CheckStatus(nodo);
}

bool Network::CheckStatus(NetworkElement & nodo) const{
	
	if(nodo.AllegedFailure || nodo.ReportedFailure )
		return false;
	else if( nodo.FailureChildrenExceed(threshold) ){
			nodo.AllegedFailure = true;
			return false;
		}

	if (nodo.parent != NULL)
		return  CheckStatus(*(nodo.parent));

	return true;
}

void Network::setThreshold(int i){
	threshold=i;
}
