/*
 * NetworkElement.h
 *
 *  Created on: May 1, 2014
 */

#ifndef NETWORKELEMENT_H_
#define NETWORKELEMENT_H_

using namespace std;
#include <iostream>
#include <set>
#include <string>
#include "NetworkElementType.h"
#include "Lista.h"
#include "Dictionary.h"
#include "../dataTypes/Diagnostics.h"

class NetworkElement {
	friend class Network;
private:

	static const char* NETWORK_ELEMENT_HUB;
	static const char* NETWORK_ELEMENT_NODE;
	static const char* NETWORK_ELEMENT_CM;
	static const char* NETWORK_ELEMENT_AMP;

	Dictionary<string, NetworkElement> children;
	string name;
	NetworkElementType type;
	bool visit;
	bool AllegedFailure;
	bool ReportedFailure;
	unsigned int numFailureChildren;
	unsigned int numChildren;

	bool isChildAlready(NetworkElement* );
	bool FailureChildrenExceed(unsigned int);

	// Diagnosis
	bool verbose;
	string prefix;

public:
	NetworkElement *parent;
    NetworkElement();
    NetworkElement(string, NetworkElementType);
	virtual ~NetworkElement();
	bool addChildren(NetworkElement &);
    bool operator<(const NetworkElement &) const;
	NetworkElementType getType()const;
	string getStringType() const;
	friend ostream &operator<<( ostream &, const NetworkElement &);

	void setVerbose();
	void unsetVerbose();
};



#endif /* NETWORKELEMENT_H_ */
