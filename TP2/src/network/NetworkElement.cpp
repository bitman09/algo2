/*
 * NetworkElement.cpp
 *
 *  Created on: May 1, 2014
 */

#include "NetworkElement.h"

using namespace std;

const char* NetworkElement::NETWORK_ELEMENT_HUB = "Hub";
const char* NetworkElement::NETWORK_ELEMENT_NODE = "Node";
const char* NetworkElement::NETWORK_ELEMENT_CM = "CM";
const char* NetworkElement::NETWORK_ELEMENT_AMP = "Amp";

NetworkElement::NetworkElement() {
    this->parent = NULL;
    this->visit = false;
    this->ReportedFailure= false;
    this->AllegedFailure= false;
    this->numFailureChildren=0;
    this->numChildren = 0;
    this->verbose = false;
}

NetworkElement::NetworkElement(string name,
                                NetworkElementType enumNetworkElementType) {

                            this->name = name;
                            this->type = enumNetworkElementType;
                            this->parent = NULL;
                            this->visit = false;
                            this->ReportedFailure= false;
                            this->AllegedFailure= false;
                            this->numFailureChildren=0;
                            this->numChildren = 0;
                            this->verbose = false;
                        }

NetworkElement::~NetworkElement() {
	/**
	 *  No se hace el borrado de los hijos (children)
	 *  porque se puede dar el caso de una
	 *  red que no sea una árbol (con loops, varios árboles,
	 *  nodos desconectados, etc.) entonces podría ser que
	 *  algunos nodos sean liberados y otros no
	 */
}


NetworkElementType NetworkElement::getType()const{
	return this->type;
}

bool NetworkElement::isChildAlready(NetworkElement* child) {

	return children.exists(child->name);
}

bool NetworkElement::addChildren(NetworkElement &child) {

    DIAG("NetworkElement::addChildren() BEGIN name : " << child.name )
	if (!children.exists(child.name)){
		children.put(child.name,child);
		this->numChildren++;
	    DIAG("NetworkElement::addChildren() END ... added ")
		return true;
	}

    DIAG("NetworkElement::addChildren() END ... not added ")
	return false;
}


bool NetworkElement::operator<(const NetworkElement & elem) const {

	return name < elem.name;
}


string NetworkElement::getStringType() const{     

	switch(this->type){
		case Hub: return NETWORK_ELEMENT_HUB; 
		case Node: return NETWORK_ELEMENT_NODE; 
		case Amp: return NETWORK_ELEMENT_AMP; 
		case CM: return NETWORK_ELEMENT_CM; 
	}	
	return "Unknown";
}


bool NetworkElement::FailureChildrenExceed(unsigned int t){
	
	if(this->numChildren)
		return ( numFailureChildren > t || this->numChildren == numFailureChildren);
	return false;

}

ostream &operator <<(  ostream& oss, const NetworkElement &e )
{
    oss << "NetworkElement{ name=" << e.name << ", type=" << e.type << " }";
    return oss;
}

void NetworkElement::setVerbose()
{
    this->verbose = true;
    children.setVerbose();
}

void NetworkElement::unsetVerbose()
{
    this->verbose = false;
    children.unsetVerbose();
}
