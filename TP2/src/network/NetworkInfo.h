/*
 * NetworkInfo.h
 *
 *  Created on: Apr 29, 2014
 */

#ifndef NETWORKINFO_H_
#define NETWORKINFO_H_

#include <iostream>
#include <string>
#include <NetworkParser.h>
#include "Network.h"

using namespace std;


class NetworkInfo {
private:
        Network *network;
        NetworkParser *report;

        bool verbose;
        string prefix;
public:
	NetworkInfo(Network *, NetworkParser * );
	NetworkInfo(NetworkInfo const&);
	virtual ~NetworkInfo();
	void print(ostream &) const;
	void reportState(istream &, ostream &, int &, string &) throw (exception);
};

#endif /* NETWORKINFO_H_ */
