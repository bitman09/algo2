/*
 * Memory.h
 *
 *  Created on: Jun 20, 2014
 */

#ifndef MEMORY_H_
#define MEMORY_H_

using namespace std;

#include <stdlib.h>
#include <string>
#include <fstream>

class Memory
{
  public:
    Memory();
    long getVM();
    long getResident();

  private:
    long readInfo(string);

    static const char* PROC_SELF_STATUS;
};

#endif /* MEMORY_H_ */
