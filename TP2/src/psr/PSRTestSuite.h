/*
 * PSRTestSuite.h
 *
 *  Created on: Jun 21, 2014
 */

#ifndef PSRTESTSUITE_H_
#define PSRTESTSUITE_H_

using namespace std;

#include <cpptest.h>
#include "Memory.h"
#include "Timer.h"
#include <queue>
#include <cstdio>
#include <string>
#include <map>

#define TEST_ADD_PSR(name,x)   registerName(name); TEST_ADD(x)

class PSRTestSuite : public Test::Suite
{
    public:
        PSRTestSuite();
        ~PSRTestSuite();

    protected:
        virtual void setup();
        virtual void tear_down();
        void startSubTest(string );
        void stopSubTest(string );
        void registerName(string);

    private:

        class SubTest {
            public:
                Timer *timer = NULL;
                long memoryVM;
                long memoryRSS;

                SubTest();
                SubTest(Timer *, long, long);
                ~SubTest();
        };

        Memory m;
        Timer t;
        long vmMem;
        long rssMem;

        map<string, SubTest> str;
        queue<string> testNames;
        string currTestName;
};

#endif /* PSRTESTSUITE_H_ */
