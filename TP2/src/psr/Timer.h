/*
 * Timer.h
 *
 *  Created on: Jun 20, 2014
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <ctime>

class Timer
{
    private:
        clock_t tStart;
        clock_t tStop;

    public:
        Timer();
        void start();
        double stop();
        double getDelta();
        double getDeltaMillis();
};

#endif /* TIMER_H_ */
