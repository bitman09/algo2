/*
 * PSRTestSuite.cpp
 *
 *  Created on: Jun 21, 2014
 */

#include "PSRTestSuite.h"

PSRTestSuite::PSRTestSuite()
{
}

void PSRTestSuite::setup()
{

    this->currTestName = this->testNames.front();
    this->testNames.pop();
    cout << "--------------------------------- Test '" << this->currTestName << "'" << endl;
    vmMem = m.getVM();
    rssMem = m.getResident();
    t.start();
}
void PSRTestSuite::tear_down()
{
    t.stop();
    cout << "Test " << this->currTestName << "," << t.getDeltaMillis() << ","
           << m.getVM() - vmMem << "," << m.getResident() - rssMem << endl;
}

PSRTestSuite::~PSRTestSuite()
{

}


void PSRTestSuite::startSubTest( string name )
{
    Timer *tr = new Timer();
    SubTest st(tr, m.getVM(), m.getResident());
    str[this->currTestName + "." + name] = st;
    tr->start();
}

void PSRTestSuite::stopSubTest( string name )
{
    std::string currSubTestName = this->currTestName + "." + name;
    Timer *t2 = str[currSubTestName].timer;
    t2->stop();
    cout << "SubTest " << currSubTestName << "," << t2->getDeltaMillis() << ","
            << m.getVM() - str[currSubTestName].memoryVM << "," << m.getResident() - str[currSubTestName].memoryRSS
            << endl;

}

PSRTestSuite::SubTest::SubTest( Timer * timer, long memVM, long memRSS )
{
    this->timer = timer;
    this->memoryVM = memVM;
    this->memoryRSS = memRSS;
}

PSRTestSuite::SubTest::SubTest()
{
}

PSRTestSuite::SubTest::~SubTest()
{
}

void PSRTestSuite::registerName( string name )
{
    this->testNames.push(name);
}
