/*
 * Memory.cpp
 *
 *  Created on: Jun 20, 2014
 */

#include "Memory.h"

#include <iostream>

const char* Memory::PROC_SELF_STATUS = "/proc/self/status";

Memory::Memory()
{
}


long Memory::readInfo( string tag )
{
    fstream is;
    string s;

    is.open(PROC_SELF_STATUS, ios::in);

    while ( ! is.eof() && s != tag )
        is >> s;

    is >> s;
    return atol(s.c_str());
}

long Memory::getVM()
{
    return readInfo("VmSize:");
}

long Memory::getResident()
{
    return readInfo("VmRSS:");
}
