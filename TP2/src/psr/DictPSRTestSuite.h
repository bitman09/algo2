/*
 * DictPSRTestSuite.h
 *
 *  Created on: Jun 21, 2014
 */

#ifndef DICTPSRTESTSUITE_H_
#define DICTPSRTESTSUITE_H_

using namespace std;

#include "PSRTestSuite.h"
#include "Dictionary.h"

class DictPSRTestSuite: public PSRTestSuite
{
    public:
        DictPSRTestSuite()
        {
            TEST_ADD_PSR("put10Mint", DictPSRTestSuite::put10Mint)
            TEST_ADD_PSR("multipleRehashes", DictPSRTestSuite::multipleRehashes)
            TEST_ADD_PSR("multipleRehashesStr", DictPSRTestSuite::multipleRehashesStr)
            TEST_ADD_PSR("put10Mstr", DictPSRTestSuite::put10Mstr)
        }


    protected:

        void tear_down()
        {
            PSRTestSuite::tear_down();
            /**
             * Si se libera la memoria antes de ser contabilizada
             * nos daría siempre que se usa muy poca memoria
             */
            if ( d != NULL) {
                delete d;
                d = NULL;
            }

            if ( dS != NULL) {
                delete dS;
                dS = NULL;
            }
        }

    private:
        Dictionary<int,int> *d = NULL;
        Dictionary<string,int> *dS = NULL;
        static const int TEN_MILLION = 10000000;
        static const int HUNDRED_THOUSAND = 100000;

        void multipleRehashes();
        void multipleRehashesStr();
        void put10Mint();
        void put10Mstr();
};

#endif /* DICTPSRTESTSUITE_H_ */
