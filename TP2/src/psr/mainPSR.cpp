/*
 * mainPSR.c
 *
 *  Created on: Jun 21, 2014
 */

#include "DictPSRTestSuite.h"

int main(int argc, char *argv[]) {
    bool errorTS;
    Test::TextOutput output(Test::TextOutput::Terse);
    DictPSRTestSuite dictts;
    errorTS = dictts.run(output);

    // Si hay errores en los tests terminar con error (devuelve 1)
    return ! errorTS;
}



