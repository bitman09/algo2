/*
 * DictPSRTestSuite.cpp
 *
 *  Created on: Jun 21, 2014
 */

#include "DictPSRTestSuite.h"

void DictPSRTestSuite::multipleRehashes()
{
    d = new Dictionary<int, int>;
    for (long int k = 1; k < HUNDRED_THOUSAND; k++) {
        int *v = new int(333);
        int *key = new int(k);
        d->put(*key, *v);
    }
}

void DictPSRTestSuite::multipleRehashesStr()
{
    dS = new Dictionary<string, int>;
    for (long int k = 1; k < HUNDRED_THOUSAND; k++) {

        char buffer[20];
        sprintf(buffer,"CM%lu",k);
        string *key = new string(buffer);
        int *v = new int(333);
        dS->put(*key, *v);
    }
}

void DictPSRTestSuite::put10Mint()
{

    startSubTest("put");

    d = new Dictionary<int, int>;
    for (long int k = 1; k < TEN_MILLION; k++) {
        int *v = new int(333);
        int *key = new int(k);
        d->put(*key, *v);
    }
    stopSubTest("put");

    startSubTest("get");
    for (long int k = 1; k < TEN_MILLION; k++) {
        TEST_ASSERT_EQUALS(333, d->get(k))
    }

    stopSubTest("get");
}

void DictPSRTestSuite::put10Mstr()
{
    unsigned long int limit = TEN_MILLION;

    startSubTest("put");
    dS = new Dictionary<string, int>;
    for (unsigned long int value = 1; value < limit; value++) {

        char buffer[20];
        sprintf(buffer,"CM%lu",value);
        string *key = new string(buffer);
        int *v = new int(value);

        dS->put(*key, *v);

    }
    stopSubTest("put");

    startSubTest("exists");
    for (unsigned long int value = 1; value < limit; value++) {

        char buffer[20];
        sprintf(buffer,"CM%lu",value);
        string key(buffer);

        TEST_ASSERT( dS->exists(key) )

    }
    stopSubTest("exists");

    startSubTest("get");
    for (unsigned long int value = 1; value < limit; value++) {

        char buffer[20];
        sprintf(buffer,"CM%lu",value);
        string key(buffer);

        TEST_ASSERT_EQUALS( value, dS->get(key) )

    }
    stopSubTest("get");

}


