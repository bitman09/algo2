/*
 * Timer.cpp
 *
 *  Created on: Jun 20, 2014
 *      Author: vicrodri
 */

#include "Timer.h"

Timer::Timer()
{
    this->tStart = 0;
    this->tStop = 0;
}

void Timer::start()
{
    this->tStart = clock();
}

double Timer::stop()
{
    this->tStop = clock();
    return getDelta();
}

double Timer::getDelta()
{
    return (double) (this->tStop - this->tStart);
}

double Timer::getDeltaMillis()
{
    return (double) (this->tStop - this->tStart) * 1000 / CLOCKS_PER_SEC;
}
