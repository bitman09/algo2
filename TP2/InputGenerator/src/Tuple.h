/*
 * Tuple.h
 *
 *  Created on: Jun 25, 2014
 *      Author: vicrodri
 */

#ifndef TUPLE_H_
#define TUPLE_H_

using namespace std;

template<class K1, class K2>
    class Tuple
    {
        public:
            Tuple( K1 &, K2 & );
            K2 second;
            K1 first;
            bool operator==( Tuple<K1, K2> ) const;
            bool operator!=( Tuple<K1, K2> ) const;

    };

template<class K1, class K2>
    Tuple<K1, K2>::Tuple( K1& f, K2& s )
    {
        first = f;
        second = s;
    }

template<class K1, class K2>
    bool Tuple<K1, K2>::operator ==( Tuple<K1, K2> t2 ) const
    {
        return (this->first == t2.first && this->second == t2.second);
    }

template<class K1, class K2>
    bool Tuple<K1, K2>::operator !=( Tuple<K1, K2>  that ) const
    {
        bool ret = !(*this == that);;
        return ret;
    }

#endif /* TUPLE_H_ */
