/*
 * InputGenerator.h
 *
 *  Created on: Apr 10, 2014
 */

#ifndef INPUTGENERATOR_H_
#define INPUTGENERATOR_H_

using namespace std;

#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdlib.h>

#include "SingleInputCreator.h"
#include "Tuple.h"

class InputGenerator {
private:
	unsigned int numHubs;
	unsigned int numNodes;
	unsigned int numAmps;
	unsigned long int numCMs;
	fstream f;

	string netName;
	list<string> hubs;
	list<string> nodes;
	list<string> amps;
	list<string> cms;
	list<Tuple<string,string> > connections;

	string input;

    static const string NODE;
    static const string AMP;
    static const string HUB;
    static const string CM;

	void generateName();
	void generateHubs();
	void generateNodes();
	void generateAmps();
	void generateCMs();
	void generateConnects();
	void nameGenerator(const string &, unsigned long int, list<string> &);
	string createName(const string &, unsigned int );
	void createConnection(const string &, unsigned int &,const  string &, unsigned int &);

public:
	InputGenerator(unsigned int, unsigned int, unsigned int, unsigned long int );
	virtual ~InputGenerator();
	void run(ostream &);
};

#endif /* INPUTGENERATOR_H_ */
