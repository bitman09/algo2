
using namespace std;

#include "InputGenerator.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>

void usage() {
    cout << "ig.exe topology_file num_hubs num_nodes num_amps num_cms"
         << endl;
    exit(0);
}

int main(int argc, char *argv[]) {

    if (argc != 6) {
        usage();
        return 1;
    }

	cout << "Input generator Running ..." << endl;
	cout << "Output file : " << argv[1] << endl;
	fstream f(argv[1], ios::out);

    cout << "Hubs : " << argv[2] << endl;
    cout << "Nodes : " << argv[3] << endl;
    cout << "Amps : " << argv[4] << endl;
    cout << "CMs : " << argv[5] << endl;
	InputGenerator generator(atoi(argv[2]),atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));
	generator.run(f);

	f.close();

	cout << "Input generator ended" << endl;
	return 0;

}

