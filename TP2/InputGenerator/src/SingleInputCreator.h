/*
 * SingleInputCreator.h
 *
 *  Created on: Apr 10, 2014
 */

#ifndef SINGLEINPUTCREATOR_H_
#define SINGLEINPUTCREATOR_H_

#include <string>
#include <list>
#include <iostream>
#include <ostream>
#include "Tuple.h"

using namespace std;

/**
 * Generación del archivo de entrada en base a los datos pasados en el constructor.
 * La inteligencia de esta clase es mínima, no hace validaciones de ningún tipo sobre
 * los datos de entrada, con lo cual debe tenerse cuidado de cumplir las precondiciones
 * establecidas
 */
class SingleInputCreator {
private:
	string netName;
	list<string>  hubs;
	list<string> nodes;
	list<string> amps;
	list<string> cms;
	list< Tuple<string,string> > connections;

	void createElements(list<string> &, const char *, const char *, ostream &);
	void createConnections(ostream &);

public:
	/**
	 * Pre-condiciones :
	 * - nombre de red : debe ser un nombre válido
	 * - nombres de los elementos : deben ser nombres válidos
	 * - cantidad de conexiones : debe haber al menos 2 elementos en toda la red
	 * 		( hubs.size() + nodes.size() + amps.size() + cms.size() ) >= 2
	 */
	SingleInputCreator(string, list<string>, list<string>, list<string>, list<string>, list< Tuple<string,string> >);
	virtual ~SingleInputCreator();
	void run(ostream &);
};

#endif /* SINGLEINPUTCREATOR_H_ */
