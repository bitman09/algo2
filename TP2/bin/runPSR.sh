#!/bin/bash

__run="bin/tp2-psr.exe"
__valgrindCmd="valgrind --leak-check=yes --track-origins=yes --read-var-info=yes"

function usage() {
    cat <<EOT

   Performance tests execution helper
   
   usage:
       $__PRG [--mem]
    
    mem : valgrind memory leaks detection
    
EOT

}

while [[ ! -z $1 ]]; do
    if [[ "$1" == "--mem" ]]; then
       shift
        __run="${__valgrindCmd} ${__run}"
    else
       showErr "Unknown option $1"
       usage
       exit -1
    fi
    shift
done

make clean psr && ${__run}
