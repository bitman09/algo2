#/bin/bash

__PRG=$(basename ${0})
__run="bin/tp2.exe"
__runPhase2=bin/tp2-tests.exe
__igRun="bin/ig.exe"
__igTestRun="bin/ig-test.exe"
__exec=${__run}
__isMemCheck=0
__testErrors=0
__testWarns=0
__testRun=0
__testMemLeaks=0
__tmp=/tmp/${__PRG}.$$
__valgrindCmd="valgrind --leak-check=yes --track-origins=yes --read-var-info=yes"
__p1=1
__p2=1
__igp1=0
__igp2=0
__dirty=0
__igRunParams="1 1 1 1"

red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
cyan="$(tput setaf 6)"
off="$(tput sgr0)"

function usage() {
    cat <<EOT

   Test execution helper
   
   usage:
       $__PRG [--mem][--p2]
    
    mem : valgrind memory leaks detection
    p2 : phase 2 only
    p1 : phase 1 only  
    dirty : avoid temp folder cleanup
    
    Input generator
    ---------------
    ig : run phase 1 and phase 2
    ig-p1 : run phase 1
    ig-p2 : run phase 2
    ig-params : command line parameters
    
EOT

}

function showMsg() {
    echo -e "$__PRG: ${cyan}$1${off}"
}

function showErr() {
    showMsg "${red}ERROR:${off} $*"
}

function showWarn() {
    showMsg "${yellow}WARNING:${off} $*"
}

function build() {
	showMsg "Building..."
	make clean mem-check
}

function doP2() {
	[[ ${__p2} -eq 0 ]] && return
	
	showMsg "Running tests..."
	make clean test && showPhase1Stats && ${__runPhase2}
}

function showValgrindOrNot() {
	local __isMemCheck=$1
	local __outNoValgrind=$2
	local __out=$3
	
	if [[ ${__isMemCheck} -eq 1 ]]; then 
		if [[ `grep --count "ERROR SUMMARY: 0 errors from 0 contexts" ${__outErr}` -eq 1 ]]; then
			cat ${__outNoValgrind}
		else
			cat ${__out}
			showErr "Pérdida de memoria detectada" 
			let __testMemLeaks=__testMemLeaks+1
		fi
	else
		cat ${__outNoValgrind}
	fi
}

function showOutput() {
	showMsg "Topology"
	local __outFile=$1
	local __outErr=$2
	local __outErrNoValgrind=$3
	local __isErrorResponse=$4
	
	if [[ ${__isErrorResponse} -eq 1 ]]; then
		## mostrar lo que hay en stderr
		showValgrindOrNot ${__isMemCheck} ${__outErrNoValgrind}  ${__outErr}
	else
		## mostrar el archivo pasado como -o
		local __tmpFile=${__tmp}/showOutput
		
		## Se debe mostrar la salida de valgrind + output 
		cat ${__outFile}  ${__outErr} > ${__tmpFile}
		showValgrindOrNot ${__isMemCheck} ${__outFile}  ${__tmpFile}
		rm -f ${__tmpFile}
	fi
}

function doCleanup() {
	[[ $__dirty -eq 1 ]] && return
	rm -rf ${__tmp}
}

function init() {
	doCleanup
	mkdir -p ${__tmp}
}

function showPhase1Stats() {
	[[ ${__p1} -eq 0 ]] && return
	
	local __memLeaksResult=${__testMemLeaks}
	[[ ${__isMemCheck} -eq 0 ]] && __memLeaksResult="No se ejecutaron"
	
	showMsg "==== Fase 1   : estadísticas ===="
	showMsg "    Ejecutados : ${__testRun}"
	
	if [[ ${__testErrors} -eq 0 ]]; then
		showMsg "       Errores : ${__testErrors}"
	else
		showMsg "${red}       Errores : ${__testErrors}${off}"
	fi
	
	showMsg "    Mem. Leaks : ${__memLeaksResult}" 
	
	if [[ ${__testWarns} -eq 0 ]]; then
		showMsg "      Warnings : ${__testWarns}"
	else
		showMsg "${yellow}      Warnings : ${__testWarns}${off}" 
	fi
	
	showMsg "---------------------------------"
}

function doDiff() {
	local __isErrorResponse=$1
	local __gold=$2
	local __outErrNoValgrind=$3
	local __out=$4
	
	if [[ ${__isErrorResponse} -eq 1 ]]; then
		diff -U 3 ${__gold} ${__outErrNoValgrind}
	else
		diff -U 3 ${__gold} ${__out}
	fi
	
	if [[ $? -ne 0 ]]; then
		showErr "Salida incorrecta"
		let __testErrors=__testErrors+1
	fi
}

function doP1() {
	[[ ${__p1} -eq 0 ]] && return
	build
	
	[[ ! -x ${__exec} ]] && showErr "Error de compilación?" && exit -1
	
	for __inputFile in `ls tests/*.in`; do
		local __fileName=`basename ${__inputFile}`
		local __out="${__tmp}/${__fileName}.out" ## salida del programa
		local __gold=${__inputFile}.gold
		local __outErr=${__tmp}/${__fileName}.stderr
		local __outErrNoValgrind=${__outErr}.clean
		local __qry=${__inputFile}.qry
		local __qryOut=${__tmp}/${__fileName}.qry.out
		local __qryOutGold=${__qry}.gold
		let __testRun=__testRun+1
		showMsg "------------------------------------------------------------------> ${__inputFile}"

		## El gold file existe sólo si existe un error
		local __isErrorResponse=0
		[[ -f ${__gold} ]] && __isErrorResponse=1
		
		if [[ $__isErrorResponse -eq 1 ]]; then
			${__run} --topology ${__inputFile} -i tests/dummyIn -o ${__out} >${__outErr} 2>&1
		else
			${__run} --topology ${__inputFile} -i ${__qry} -o ${__qryOut} --print-topology ${__out} >${__outErr} 2>&1
		fi
	
		
		grep -v "^==" ${__outErr} > ${__outErrNoValgrind}
		
		showOutput ${__out} ${__outErr} ${__outErrNoValgrind} ${__isErrorResponse}
				
		showMsg "Topology Diff"
		## si no es un error entonces el gold file es la entrada
		local __outSorted=${__out}.sorted	
		if [[ ${__isErrorResponse} -eq 0 ]]; then
			__gold=${__tmp}/${__fileName}.gold
			sort ${__inputFile} > ${__gold}
			sort ${__out} > ${__outSorted}
		fi
	
		doDiff $__isErrorResponse $__gold $__outErrNoValgrind $__outSorted
        
		if [[ ${__isErrorResponse} -eq 0 ]]; then
			showMsg "Query Output"
			cat ${__qryOut}
			
			showMsg "Query Diff"
			diff -U 3 ${__qryOutGold} ${__qryOut} 
				
			if [[ $? -ne 0 ]]; then
				showErr "Salida incorrecta"
				let __testErrors=__testErrors+1
			fi
		fi
		
	done

	[[ ${__p2} -eq 0 ]] && showPhase1Stats
}

function doIGP1() {
	[[ $__igp1 -eq 0 ]] && return
	
	showMsg "Input Generator, Unit tests"
	cd InputGenerator && make clean test && ${__igTestRun} && cd -
	
	showMsg "Input Generator, fase 2 FIN"

}

function doIGP2() {
	[[ $__igp2 -eq 0 ]] && return
	
	local __topology="${__tmp}/topology"
	showMsg "Input Generator, Integration tests"
	cd InputGenerator && make clean all
		
	if [[ $? -ne 0 ]]; then
		cd -
		showErr "Algo salió mal. Compilación?"
		let __testErrors=__testErrors+1
		return
	fi
	
	${__igRun} ${__topology} ${__igRunParams}  && cd - \
		&& make clean all 
		
	if [[ $? -ne 0 ]]; then
		showErr "Algo salió mal. Generación?"
		let __testErrors=__testErrors+1
		return
	fi

	local __topologyOK="${__tmp}/topology.ok"
	${__run} --topology ${__topology} --print-topology ${__topologyOK}
	__isError=`grep --count "^error" ${__topologyOK}`
	
		
	if [[ $__isError -gt 0 ]]; then
		showErr "Topología incorrecta"
		cat ${__topology}
		let __testErrors=__testErrors+1
		return
	fi
	
	showMsg "Input Generator, fase 2 FIN"

}

while [ $# -gt 0 ]
do
        case "$1" in
                --mem)
					__run="${__valgrindCmd} ${__run}"
					__runPhase2="${__valgrindCmd} ${__runPhase2}"
					__igRun="${__valgrindCmd} ${__igRun}"
					__igTestRun="${__valgrindCmd} ${__igTestRun}"
					__isMemCheck=1
                    ;;
                --p2)
					__p1=0
       				__p2=1
                    ;;
                --p1)
					__p1=1
       				__p2=0
                    ;;
				--ig)
					__igp1=1
					__igp2=1
					__p1=0
       				__p2=0
					;;
				--ig-p1)
					__igp1=1
					__igp2=0
					__p1=0
       				__p2=0
					;;
				--ig-p2)
					__igp1=0
					__igp2=1
					__p1=0
       				__p2=0
					;;
				--ig-params)
					shift
					__igRunParams=$1
					;;
				--dirty)
					__dirty=1
					;;
                *)  
                	showErr "Unknown option $1"
                	usage
                	exit -1
                	;;
    esac
    shift
done

init
doP1
doP2
doIGP1
doIGP2
doCleanup
