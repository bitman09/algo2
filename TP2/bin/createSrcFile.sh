#!/bin/bash


__PRG=$(basename ${0})


red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
cyan="$(tput setaf 6)"
off="$(tput sgr0)"

function usage() {
	[[ ! -z $1 ]] && echo "" &&  showErr $1
	
    cat <<EOT

   Test execution helper
   
   usage:
       $__PRG --lst fileList --out txtOutput
    
    lst : input file containing files to put in output file
    out : output file to put fileList files into
    
EOT

}

function showMsg() {
    echo -e "$__PRG: ${cyan}$1${off}"
}

function showErr() {
    showMsg "${red}ERROR:${off} $*"
}

function validateInput() {
	[[ "${__lst}" == "" ]] && \
		usage "No input file" && \
		exit -1
		
	[[ ! -f ${__lst} ]] && \
		showErr "No existe el archivo de entrada ($__lst)" && \
		exit -1
}

function printLine() {
	 local __s=$1
	 local __rep=$2
	 local __aux=$(printf "%-${__rep}s" "$__s")
	 echo "${__aux// /=}"  >> ${__out}
}

function doCreate() {
	rm -f ${__out} && touch ${__out}  
	for __file in `cat ${__lst}`; do
		echo ${__file} >> ${__out} 
		
		__chars=`echo ${__file} | wc --chars`
		printLine "=" ${__chars} 
		
		echo -e "\n" >> ${__out} 
		cat ${__file} >>  ${__out}
		echo -e "\x0C" >> ${__out} 
	done
}

while [[ ! -z $1 ]]; do
    if [[ "$1" == "--lst" ]]; then
       shift
       __lst=$1
    elif [[ "$1" == "--out" ]]; then
       shift
       __out=$1
    else
       showErr "Unknown option $1"
       usage
       exit -1
    fi
    shift
done

validateInput
doCreate