/*
 * NetworkElementTestSuite.cpp
 *
 *  Created on: May 18, 2014
 */

#include "NetworkElementTestSuite.h"

void NetworkElementTestSuite::addSameChildren() {
	NetworkElement parent("parent", Node);
	NetworkElement child("child", Node);
	TEST_THROWS_NOTHING( parent.addChildren(&child) );
	TEST_ASSERT( ! parent.addChildren(&child) );
}

void NetworkElementTestSuite::addChildrenSameName() {
	NetworkElement parent("parent", Node);
	NetworkElement child("child", Node);
	NetworkElement child2("child", Node);
	TEST_THROWS_NOTHING( parent.addChildren(&child) );
	TEST_ASSERT( ! parent.addChildren(&child2) );
}

void NetworkElementTestSuite::addChildrenDifferentName() {
	NetworkElement parent("parent", Node);
	NetworkElement child("child", Node);
	NetworkElement child2("child2", Node);
	TEST_THROWS_NOTHING( parent.addChildren(&child) );
	TEST_ASSERT( parent.addChildren(&child2) );
}
