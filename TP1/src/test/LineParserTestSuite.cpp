/*
 * LineParserTestSuite.cpp
 *
 *  Created on: May 18, 2014
 */

#include "LineParserTestSuite.h"

void LineParserTestSuite::comandoUnico() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser("AAAA bbbbb");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoSinParams() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser("AAAA");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoConEspacio() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser(" AAAA");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoConEspacioAlfinal() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser("AAAA ");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoParametro() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser("AAAA bbbbb");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(1, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	delete lp;
}

void LineParserTestSuite::comando2Parametros() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser("AAAA bbbbb ccc");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(2, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	TEST_ASSERT_EQUALS("ccc", (*p)[1]);
	delete lp;
}

void LineParserTestSuite::comandoConEspaciosAlFinal() {
	NetworkParser::LineParser *lp = new NetworkParser::LineParser("AAAA bbbbb ccc  ");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(2, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	TEST_ASSERT_EQUALS("ccc", (*p)[1]);
	delete lp;
}

