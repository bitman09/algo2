/*
 * tests.cpp
 *
 *  Created on: Mar 27, 2014
 */

#include "testsFileParser.h"

void FileParserTestSuite::networkNameMal() {

	string inS = "NetworkNameMAL pepito";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");

	TEST_THROWS(fp.parse(), WrongCommandException);
}


void FileParserTestSuite::networkNameMalParams() {

	string inS = "NetworkName pepito tiene hambre";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");

	TEST_THROWS(fp.parse(), InvalidParametersException);
}

void FileParserTestSuite::networkName() {
	string inS = "NetworkName pepito";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING( fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
}

void FileParserTestSuite::networkElement() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumConnections())
}

void FileParserTestSuite::networkElementMal() {

	string inS = "NetworkName pepito\nNetworkElementMal H1 Hub";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp.parse(), WrongCommandException)
}

void FileParserTestSuite::networkElement2() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nNetworkElement N2 Node\nConnection N1 H1\nConnection N2 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(2, fp.network->getNumNodes())
	TEST_ASSERT_EQUALS(2, fp.network->getNumConnections())
}

void FileParserTestSuite::networkElementCountHubs() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumAmps())
	TEST_ASSERT_EQUALS(0, fp.network->getNumCMs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumNodes())
	TEST_ASSERT_EQUALS(0, fp.network->getNumConnections())
}

void FileParserTestSuite::networkElementCountAmps() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement A1 Amp\nConnection A1 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(1, fp.network->getNumAmps())
	TEST_ASSERT_EQUALS(0, fp.network->getNumCMs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumNodes())
	TEST_ASSERT_EQUALS(1, fp.network->getNumConnections())
}


void FileParserTestSuite::networkElementCountCMs() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement C1 CM\nConnection C1 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumAmps())
	TEST_ASSERT_EQUALS(1, fp.network->getNumCMs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumNodes())
	TEST_ASSERT_EQUALS(1, fp.network->getNumConnections())
}

void FileParserTestSuite::networkElementCountNodes() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection N1 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(0, fp.network->getNumAmps())
	TEST_ASSERT_EQUALS(0, fp.network->getNumCMs())
	TEST_ASSERT_EQUALS(1, fp.network->getNumNodes())
	TEST_ASSERT_EQUALS(1, fp.network->getNumConnections())
}

void FileParserTestSuite::networkElementEstaDuplicado() {

	string inS = "NetworkName pepito\nNetworkElement H1 Node\nNetworkElement H1 Node";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "duplicated network element")
}



void FileParserTestSuite::networkElementTipoInvalido() {

	string inS = "NetworkName pepito\nNetworkElement H1 TIPO_INVALIDO";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp.parse(), WrongNetworkelementTypeException);
}

void FileParserTestSuite::connection() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection N1 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(1, fp.network->getNumConnections())
}

void FileParserTestSuite::connection2() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nNetworkElement N2 Node\nConnection N1 H1\nConnection N2 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_NOTHING(fp.parse() )
	TEST_ASSERT_EQUALS("pepito", fp.network->getName())
	TEST_ASSERT_EQUALS(1, fp.network->getNumHubs())
	TEST_ASSERT_EQUALS(2, fp.network->getNumConnections())
}

void FileParserTestSuite::connectionMal() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnectionMal N1 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp.parse(), WrongCommandException)
}

void FileParserTestSuite::connectionMix() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection N1 H1\nNetworkElement N2 Node";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp.parse(), UnexpectedCommandException);
}

void FileParserTestSuite::connectionCmdNoExiste() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection N1 H1\nXXXXXXXXX N2 Node";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp.parse(), WrongCommandException);
}

void FileParserTestSuite::connectionCmdFueraDeOrden() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkName OtraVezPepito";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS(fp.parse(), UnexpectedCommandException);
}

void FileParserTestSuite::connectionNewtorkElementNoDeclarado() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection N2 H1";


	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "undeclared network element");
}

void FileParserTestSuite::coonnectionTwoParents() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nNetworkElement N2 Node\nConnection N2 H1\nConnection N2 N1";


	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "two parents not allowed:");
}

void FileParserTestSuite::connectionMultipleHubs() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement Hub2 Hub";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "only one hub allowed");
}

void FileParserTestSuite::connectionDuplicated() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection N1 H1\nConnection N1 H1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "duplicated connection:");
}


void FileParserTestSuite::hubNoInboundConnection() {

	string inS = "NetworkName pepito\nNetworkElement H1 Hub\nNetworkElement N1 Node\nConnection H1 N1";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "a Hub must have outbound connections only");
}


void FileParserTestSuite::noHub() {

	string inS = "NetworkName pepito\nNetworkElement N1 Node";

	stringstream inSs(inS);
	NetworkParser fp(inSs, "noImportaElNombreDelArchivo");
	TEST_THROWS_WHAT_CONTAINS(fp.parse(), BaseTPException, "No hub declared");
}


