/*
 * ListaTestSuite.cpp
 *
 *  Created on: May 19, 2014
 */

#include "ListaTestSuite.h"

void ListaTestSuite::nuevaLista()
{
	LinkedList<int> l;
	TEST_ASSERT(l.empty())
	TEST_ASSERT(l.length() == 0)
}

void ListaTestSuite::nuevaListaBorrar()
{
	LinkedList<int> l;
	TEST_ASSERT(! l.erase(0))
	TEST_ASSERT(l.empty())
	TEST_ASSERT(l.length() == 0)
}

void ListaTestSuite::unElemPrincipio()
{
	LinkedList<int> l;
	l.insert(2);
	TEST_ASSERT(!l.empty())
	TEST_ASSERT(l.length() == 1)
}

void ListaTestSuite::unElemPrincipioBorrar()
{
	LinkedList<int> l;
	l.insert(2);
	TEST_ASSERT(l.length() == 1)

	TEST_ASSERT(l.erase(0))
	TEST_ASSERT(l.length() == 0)

	TEST_ASSERT(! l.erase(0))
	TEST_ASSERT(l.length() == 0)
}

void ListaTestSuite::unElemFinal()
{
	LinkedList<int> l;
	l.add(2);
	TEST_ASSERT(!l.empty())
	TEST_ASSERT(l.length() == 1)
}


void ListaTestSuite::unElemFinalBorrar()
{
	LinkedList<int> l;
	l.add(2);
	TEST_ASSERT(l.length() == 1)

	TEST_ASSERT(l.erase(0))
	TEST_ASSERT(l.length() == 0)

	TEST_ASSERT(! l.erase(0))
	TEST_ASSERT(l.length() == 0)
}


void ListaTestSuite::dosElemPrincipio()
{
	LinkedList<int> l;
	l.insert(2);
	l.insert(2);
	TEST_ASSERT(!l.empty())
	TEST_ASSERT(l.length() == 2)
}


void ListaTestSuite::dosElemPrincipioBorrar()
{
	LinkedList<int> l;
	l.insert(2);
	l.insert(2);
	TEST_ASSERT(l.length() == 2)

	TEST_ASSERT(l.erase(1))
	TEST_ASSERT(l.length() == 1)
}


void ListaTestSuite::dosElemFinal()
{
	LinkedList<int> l;
	l.add(2);
	l.add(2);
	TEST_ASSERT(!l.empty())
	TEST_ASSERT(l.length() == 2)
}

void ListaTestSuite::dosElemFinalBorrar()
{
	LinkedList<int> l;
	l.add(2);
	l.add(2);
	TEST_ASSERT(l.length() == 2)

	TEST_ASSERT(l.erase(1))
	TEST_ASSERT(l.length() == 1)
}


void ListaTestSuite::buscarVacia()
{
	LinkedList<int> l;
	TEST_ASSERT(l.search(2) == NULL)
}

void ListaTestSuite::buscarLlena()
{
	LinkedList<int> l;
	l.add(2);
	l.add(3);

	int *value = l.search(2);
	TEST_ASSERT( *value == 2)
}

void ListaTestSuite::buscarLlenaYnoHalar()
{
	LinkedList<int> l;
	l.add(2);
	l.add(3);
	int *value = l.search(4);
	TEST_ASSERT( value == NULL)
}

void ListaTestSuite::itEmpty()
{
	LinkedList<int> l;
	LinkedList<int>::Iterator it(l);
	TEST_ASSERT( it.end() )
}

void ListaTestSuite::itOneElement()
{
	LinkedList<int> l;
	l.add(1);
	LinkedList<int>::Iterator it(l);
	TEST_ASSERT( ! it.end() )
	TEST_ASSERT_EQUALS( 1, it.elem() )
	it++;
	TEST_ASSERT( it.end() )
}

void ListaTestSuite::itMoreElements()
{
	LinkedList<int> l;
	l.add(1);
	l.add(2);
	l.add(14);
	LinkedList<int>::Iterator it(l);
	TEST_ASSERT( ! it.end() )
	TEST_ASSERT_EQUALS( 1, it.elem() )
	it++;
	TEST_ASSERT( ! it.end() )
	TEST_ASSERT_EQUALS( 2, it.elem() )
	it++;
	TEST_ASSERT( ! it.end() )
	TEST_ASSERT_EQUALS( 14, it.elem() )
	it++;
	TEST_ASSERT( it.end() )
}
