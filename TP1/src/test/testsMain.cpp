/*
 * testMain.cpp
 *
 *  Created on: Apr 26, 2014
 */

#include "testsFileParser.h"
#include "LineParserTestSuite.h"
#include "NetworkTestSuite.h"
#include "NetworkElementTestSuite.h"
#include "ListaTestSuite.h"

int main(int argc, char *argv[]) {
	bool errorTS;
	Test::TextOutput output(Test::TextOutput::Verbose);
	LineParserTestSuite ets;
	errorTS = ets.run(output);
	FileParserTestSuite fpts;
	errorTS &= fpts.run(output);
	NetworkTestSuite nts;
	errorTS &= nts.run(output);
	NetworkElementTestSuite nets;
	errorTS &= nets.run(output);
    ListaTestSuite lts;
    errorTS &= lts.run(output);

	// Si hay errores en los tests terminar con error (devuelve 1)
	return ! errorTS;
}



