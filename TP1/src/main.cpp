#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cstdlib>

#include "cmdline.h"
#include "NetworkParser.h"
#include "NetworkInfo.h"


using namespace std;


////////////////////////////////////////////////////////////////////////////
//FUNCTION PROTOTYPES

static void opt_input(string const &);
static void opt_output(string const &);
static void opt_help(string const &);

////////////////////////////////////////////////////////////////////////////
//GLOBAL VARIABLES




static option_t options[] = {
	{1, "i", "input", "-", opt_input, OPT_DEFAULT},
	{1, "o", "output", "-", opt_output, OPT_DEFAULT},
	{0, "h", "help", NULL, opt_help, OPT_DEFAULT},
	{0, },//Indica el final del arreglo
};
static istream *iss = 0; // puntero a: cin o al archivo de entrada
static string isName;    // Nombre del archivo de entrada
static ostream *oss = 0; // puntero a: cout o al archivo de salida
static fstream ifs; 
static fstream ofs;



////////////////////////////////////////////////////////////////////////////


int main(int argc, char *argv[])
{

	cmdline cmdl(options); //creo el objeto cmdl
	cmdl.parse(argc, argv); // parse de los argumentos de línea de comando

	try {
	/**
	 * SPEC : El programa deberá barrer el archivo de topología una única vez, 
	 * y construir el modelo de la red en memoria detectando, y 
	 * diagnosticando los errores (loops, elementos repetidos o inexistentes,
	 * segmentos inconexos, etc).
	 * A continuación, deberá imprimir por salida estándar la descripción
	 * de la topología de la red, generada a partir del modelo.
	 **/

		NetworkParser p(*iss, isName);
		NetworkInfo info = p.parse();
		info.print(*oss);
	}
	catch(exception &e) {
			cerr << "error: " << e.what() << endl;
			return 1;
	}


	return 0;
}



////////////////////////////////////////////////////////////////////////////

static void opt_input(string const &arg)
{
	// Si el nombre del archivos es "-", usaremos la entrada
	// estándar. De lo contrario, abrimos un archivo en modo
	// de lectura.
	//
	if (arg == "-") {
		iss = &cin;
	} else {
		ifs.open(arg.c_str(), ios::in);
		iss = &ifs;
	}
	isName = (arg == "-") ? "stdin" : arg;

	// Verificamos que el stream este OK.
	//
	if (!iss->good()) {
		cerr << "cannot open "
		     << arg
		     << "."
		     << endl;
		exit(1);
	}
}

static void opt_output(string const &arg)
{
	// Si el nombre del archivos es "-", usaremos la salida
	// estándar. De lo contrario, abrimos un archivo en modo
	// de escritura.
	//
	if (arg == "-") {
		oss = &cout;
	} else {
		ofs.open(arg.c_str(), ios::out);
		oss = &ofs;
	}

	// Verificamos que el stream este OK.
	//
	if (!oss->good()) {
		cerr << "cannot open "
		     << arg
		     << "."
		     << endl;
		exit(1);
	}
}

static void opt_help(string const &arg)
{
	cout << "tp0.exe [-i file] [-o file]"
	     << endl;
	exit(0);
}

////////////////////////////////////////////////////////////////////////////



