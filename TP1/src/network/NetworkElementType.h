/*
 * NetworkElementType.h
 *
 *  Created on: May 1, 2014
 */

#ifndef NETWORKELEMENTTYPE_H_
#define NETWORKELEMENTTYPE_H_

using namespace std;

enum NetworkElementType {
	Hub, Amp, CM, Node
};

#endif /* NETWORKELEMENTTYPE_H_ */
