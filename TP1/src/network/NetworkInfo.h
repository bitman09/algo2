/*
 * NetworkInfo.h
 *
 *  Created on: Apr 29, 2014
 */

#ifndef NETWORKINFO_H_
#define NETWORKINFO_H_

#include <iostream>
#include "Network.h"
using namespace std;

class NetworkInfo {
private:
        Network *network;
public:
	NetworkInfo(Network *);
	NetworkInfo(NetworkInfo const&);
	virtual ~NetworkInfo();
	void print(ostream &) const;
};

#endif /* NETWORKINFO_H_ */
