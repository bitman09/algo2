/*
 * Network.cpp
 *
 *  Created on: May 1, 2014
 */

#include "Network.h"

using namespace std;

const char* Network::NETWORK_NAME = "NetworkName";
const char* Network::CONNECTION = "Connection";
const char* Network::NETWORK_ELEMENT = "NetworkElement";

Network::Network() {
	this->name = "";
	this->numAmp = 0;
	this->numCM = 0;
	this->numConnectionElements = 0;
	this->numHubs = 0;
	this->numNodes = 0;
	this->root = NULL;
}

Network::~Network() {
	/**
	 * Sería mucho más interesante, y una implementación mucho
	 * más elegante, que cada NetworkElement (en el destructor)
	 * el libere la memoria de sus hijos, y así sucesivamente en
	 * forma recursiva. De esta forma liberando la memoria del Hub
	 * que está como root del árbol desencadena el borrado del
	 * árbol completo. El problema es que puede ser que la estructura
	 * formada no sea un árbol, por lo tanto para asegurarnos que
	 * se libere toda la memoria se borran cada uno de los NetworkElement
	 * registrados y no se implementa el borrado de los hijos en
	 * el destructor de NetworkElement.
	 */
	LinkedList<NetworkElement*>::Iterator it(this->networkElemContainer);
	while( ! it.end() ){
		NetworkElement* aux = it.elem();
		it++;
		delete aux;
	}
}

NetworkElement* Network::buscar(const string name) {
	LinkedList<NetworkElement*>::Iterator it(networkElemContainer);

	for(; ! it.end(); it++) {
		if ( it.elem()->name == name )
			return it.elem();
	}

	return NULL;
}
bool Network::isNetworkElementDeclared(const string name) {
	return (buscar(name) != NULL);
}

void Network::add(string name, NetworkElementType elemType) throw(exception) {

	if ( isNetworkElementDeclared(name) ) {
		throw DuplicatedNetworkElementException(name);
	}
	
	NetworkElement *node = new NetworkElement(name,elemType);

	networkElemContainer.add(node);

	switch (elemType) {
		/**
		 *  SPEC A lo largo de este trabajo adoptaremos un modelo 
		 * minimalista para representar redes HFC en nuestros sistemas, 
		 * consistiendo de un único hub en la raíz
		 **/
		case Hub : 	if( this->numHubs == 0 ) {
					this->numHubs++;
					this->root = node;
				} else {
				  throw MultipleHubsException();
				}
				break;

		case Node :	this->numNodes++;
					break;

		case CM: 	this->numCM++;
					break;

		case Amp: 	this->numAmp++;
					break;
	}
}

void Network::connect(string parentName, string childName) throw(exception) {

	if ( ! isNetworkElementDeclared(parentName) ) {
		throw NetworkElementNoDeclarado(parentName);
	}

	if ( ! isNetworkElementDeclared(childName) )  {
		throw NetworkElementNoDeclarado(childName);
	}

	this->numConnectionElements++;
	
	NetworkElement* parent = buscar(parentName);
	NetworkElement* child = buscar(childName);

	if ( parentName == childName) {
		throw LoopException();
	} else if ( ! parent->addChildren(child) ) {
		throw DuplicatedConnectionException(parent->name, child->name);
	} else if ( child->parent != NULL ) {
		throw TwoParentsException(child->parent->name, parentName, childName);
	} else if( child->getType() ==  Hub ) {
		throw HubNoInboundConnectionsException();
	} else if( parent->getType() == CM ) {
	/**
	 * SPEC
	 * 1.- "los equipos alojados en el ámbito de los clientes en las hojas (cable modem)."
	 * 2.- "A lo largo de este trabajo adoptaremos un modelo minimalista para 
	 *      representar redes HFC en nuestros sistemas, consistiendo  ... y dispositivos
	 *      ubicados en el ámbito de los clientes (cable modems)."
	 **/
		throw CableModemNoOutboundConnectionsException();
	} else {
		child->parent = parent;
	}
}

string Network::getName() const {
	return this->name;
}

void Network::verify() {
	unsigned int total;	
	
	if(numHubs == 0)
		throw NoHubException();
	else
		total = this->recorrer(root);
	
	if (total != numNodes+numCM+numAmp+numHubs)
		throw NonFullyConnectedNetworkException();

}

unsigned int Network::recorrer (NetworkElement* aux){
	
	unsigned int sumNodos=0;
	if(aux->visit==true)
		/**
		 * Los loops se detectan por construcción si quiero asignar a
		 * un padre a un nodo que ya tiene padre. Igualmente podría generar
		 * un loop pero en este, para que sea visitado por este algoritmo
		 * que empieza  recorrer desde el root, este debería ser hijo de
		 * alguien lo cual está prohibido por construcción (se verifica al
		 * conectar 2 nodos que un Hub no pueda ser hijo)
		 * Esta verificación, por lo tanto, es superflua en el contexto
		 * de este TP y sólo por la forma de construcción de la red,
		 * pero igualmente este código no fue quitado sólo por generalidad
		 * de la solución, y para que pueda ser usado fuera del contexto
		 * de este TP.
		 */
		throw LoopException();
	else{
		aux->visit=true;
	}
	
	for(LinkedList<NetworkElement*>::Iterator it(aux->children); ! it.end() ; it++) {
			sumNodos += this->recorrer(it.elem());
	}

	return sumNodos +1;
}





void Network::setName(const string name) {
	this->name = name;
}

unsigned int Network::getNumConnections() const {
	return this->numConnectionElements;
}

unsigned int Network::getNumHubs() const {
	return this->numHubs;
}

unsigned int Network::getNumNodes() const {
	return this->numNodes;
}

unsigned int Network::getNumCMs() const {
	return this->numCM;
}

unsigned int Network::getNumAmps() const {
	return this->numAmp;
}

void Network::outGeneratedInput(ostream& oss) {

	oss << NETWORK_NAME << " " << this->getName() << endl;


	for(LinkedList<NetworkElement*>::Iterator it(this->networkElemContainer);
			! it.end(); it++ ){
		oss << NETWORK_ELEMENT << " " <<  it.elem()->name << " " << it.elem()->getStringType() << endl;
	}

	for(LinkedList<NetworkElement*>::Iterator it(this->networkElemContainer);
				! it.end(); it++ ){
		if( it.elem()->getType()  != Hub ) { // HUB no tiene padre
			oss  << CONNECTION << " " << it.elem()->name << " " << it.elem()->parent->name << endl;
		}
	}

}
