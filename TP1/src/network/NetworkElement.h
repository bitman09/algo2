/*
 * NetworkElement.h
 *
 *  Created on: May 1, 2014
 */

#ifndef NETWORKELEMENT_H_
#define NETWORKELEMENT_H_

using namespace std;
#include <iostream>
#include <set>
#include <string>
#include "NetworkElementType.h"
#include "Lista.h"

class NetworkElement {
	friend class Network;
private:

	static const char* NETWORK_ELEMENT_HUB;
	static const char* NETWORK_ELEMENT_NODE;
	static const char* NETWORK_ELEMENT_CM;
	static const char* NETWORK_ELEMENT_AMP;

	LinkedList<NetworkElement*> children;
	string name;
	NetworkElementType type;
	bool visit;

	bool isChildAlready(NetworkElement* );
public:
	NetworkElement *parent;
	NetworkElement(string, NetworkElementType);
	virtual ~NetworkElement();
	bool addChildren(NetworkElement *);
    bool operator<(const NetworkElement &) const;
	NetworkElementType getType()const;
	string getStringType() const;
};



#endif /* NETWORKELEMENT_H_ */
