/*
 * Network.h
 *
 *  Created on: May 1, 2014
 */

#ifndef NETWORK_H_
#define NETWORK_H_

using namespace std;

#include <string>
#include <iostream>
#include "NetworkElement.h"
#include "Exceptions.h"
#include "Lista.h"

class Network {
private:

	static const char* NETWORK_NAME;
	static const char* CONNECTION;
	static const char* NETWORK_ELEMENT;

	LinkedList<NetworkElement*> networkElemContainer;
	
	NetworkElement* root;
	string name;
	unsigned int numConnectionElements;
	unsigned int numHubs;
	unsigned int numNodes;
	unsigned int numCM;
	unsigned int numAmp;
	
	bool isNetworkElementDeclared( const string );
	NetworkElement* buscar(const string );
	unsigned int recorrer (NetworkElement*);
public:
	Network();
	Network(const Network& that);
	virtual ~Network();
	void setName(const string);
	void add(string, NetworkElementType) throw(exception);
	void connect(string, string) throw(exception);
	string getName() const;
	unsigned int getNumConnections() const;
	unsigned int getNumHubs() const;
	unsigned int getNumNodes() const;
	unsigned int getNumCMs() const;
	unsigned int getNumAmps() const;
	void verify();
	void outGeneratedInput(ostream&);
};

#endif /* NETWORK_H_ */
