/*
 * NetworkElement.cpp
 *
 *  Created on: May 1, 2014
 */

#include "NetworkElement.h"

using namespace std;

const char* NetworkElement::NETWORK_ELEMENT_HUB = "Hub";
const char* NetworkElement::NETWORK_ELEMENT_NODE = "Node";
const char* NetworkElement::NETWORK_ELEMENT_CM = "CM";
const char* NetworkElement::NETWORK_ELEMENT_AMP = "Amp";

NetworkElement::NetworkElement(string name,
		NetworkElementType enumNetworkElementType) {

	this->name = name;
	this->type = enumNetworkElementType;
	this->parent = NULL;
	this->visit = false;
}

NetworkElement::~NetworkElement() {
	/**
	 *  No se hace el borrado de los hijos (children)
	 *  porque se puede dar el caso de una
	 *  red que no sea una árbol (con loops, varios árboles,
	 *  nodos desconectados, etc.) entonces podría ser que
	 *  algunos nodos sean liberados y otros no
	 */
}


NetworkElementType NetworkElement::getType()const{
	return this->type;
}

bool NetworkElement::isChildAlready(NetworkElement* child) {
	LinkedList<NetworkElement*>::Iterator it(children);

	for( ; ! it.end(); it++) {
		if ( it.elem()->name == child->name) {
			return true;
		}
	}

	return false;
}

bool NetworkElement::addChildren(NetworkElement* child) {
	if(children.search(child)  == NULL && ! isChildAlready(child)){
		children.add(child);
	    return true;
	} else {
		return false;		
	}
}


bool NetworkElement::operator<(const NetworkElement & elem) const {

	return name < elem.name;
}


string NetworkElement::getStringType() const{     

	switch(this->type){
		case Hub: return NETWORK_ELEMENT_HUB; 
		case Node: return NETWORK_ELEMENT_NODE; 
		case Amp: return NETWORK_ELEMENT_AMP; 
		case CM: return NETWORK_ELEMENT_CM; 
	}	
	return "Unknown";
}


