/*
 * NetworkInfo.cpp
 *
 *  Created on: Apr 29, 2014
 */

#include "NetworkInfo.h"

NetworkInfo::~NetworkInfo() {
}

NetworkInfo::NetworkInfo(const NetworkInfo& that) {
	if (this != &that) {
		this->network = that.network;
	}
}

NetworkInfo::NetworkInfo( Network * network) {
	this->network = network;
}

void NetworkInfo::print(ostream& oss) const {
	this->network->outGeneratedInput(oss);
}
