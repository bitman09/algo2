/*
 * NetworkParser.cpp
 *
 *  Created on: Mar 23, 2014
 */

#include "NetworkParser.h"

const char* NetworkParser::CMD_NETWORK_NAME = "NetworkName";
const char* NetworkParser::CMD_CONNECTION = "Connection";
const char* NetworkParser::CMD_NETWORK_ELEMENT = "NetworkElement";
const char* NetworkParser::NETWORK_ELEMENT_HUB = "Hub";
const char* NetworkParser::NETWORK_ELEMENT_NODE = "Node";
const char* NetworkParser::NETWORK_ELEMENT_CM = "CM";
const char* NetworkParser::NETWORK_ELEMENT_AMP = "Amp";

NetworkParser::NetworkParser(istream &i, const string fileName) {
	in = &i;
	this->fileName = fileName;
	this->line = 1;
	this->parsingState = STATE_NO_INICIADO;
	this->network = new Network();
}

NetworkParser::NetworkParser(const NetworkParser & fp) {
	this->line = fp.line;
	this->network = fp.network;
	this->in = fp.in;
	this->fileName = fp.fileName;
	this->parsingState = fp.parsingState;
}

NetworkParser::~NetworkParser() {
	if (this->network != NULL)
		delete this->network;
}

/**
 * Convierte un string, conteniendo un network element válido,
 * a un NetworkElementType.
 * procondición : netlement debe ser un NetworkElement válido
 */
NetworkElementType NetworkParser::converCommandtToNetElemType(
		string netElement) {

	if (netElement == NETWORK_ELEMENT_AMP) {
		return Amp;
	} else if (netElement == NETWORK_ELEMENT_CM) {
		return CM;
	} else if (netElement == NETWORK_ELEMENT_HUB) {
		return Hub;
	} else {
		// netElement == NETWORK_ELEMENT_NODE
		return Node;
	}
}

NetworkInfo NetworkParser::parse() throw (exception) {
	/**
	 *  SPEC : en la primera línea aparecerá la palabra NetworkName
	 *  seguida del nombre de la red
	 */
	readNetworkName();
	/**
	 * SPEC : El programa deberá barrer el archivo de topología
	 * una única vez ..."
	 */
	if (!in->eof()) {
		/**
		 * SPEC : Comenzando en la segunda línea , tenemos una secuencia de
		 * declaración de nodos, indicados con la palabra NetworkElement...
		 */
		readNetworkElements();
	}

	/**
	 * Networkinfo debe usarse en el mismo scope que NetworkParser,
	 * de otra forma cuando NetworkInfo consulte a Network este puede
	 * haber sido destruido.
	 * Se hizo de esta forma porque se usa el constructor de NetworkInfo
	 * que espera una referencia a Network, de lo contrario se debería
	 * copiar toda la red (si se usara un constructor por copia) y el
	 * uso de memoria se duplica.
	 */
	NetworkInfo ret(this->network);
	return ret;
}

void NetworkParser::validateCommand(const LineParser & lp) const
		throw (exception) {

	string cmd(lp.getCommand());

	// Valida que sean comandos conocidos
	if (cmd != CMD_NETWORK_ELEMENT && cmd != CMD_CONNECTION
			&& cmd != CMD_NETWORK_NAME) {
		throw WrongCommandException(cmd, this->line, this->fileName);
	}
	// En esta fase del parsing no tiene cabida el comando NetworkName
	if (cmd == CMD_NETWORK_NAME) {
		throw UnexpectedCommandException(cmd, this->line, this->fileName);
	}

	/**
	 * SPEC
	 * Después de las instancias NetworkElement, aparecen enumeradas
	 * las conexiones entre los mismos, dadas por Connection...
	 */
	if (this->parsingState == STATE_CONNECTION && cmd == CMD_NETWORK_ELEMENT) {
		throw UnexpectedCommandException(cmd, this->line, this->fileName);
	}
}

void NetworkParser::readNetworkElements() throw (exception) {

	this->parsingState = STATE_NETWORKELEMENT;

	while (!in->eof()) {

		string line;
		getline(*in, line);

		if (line.empty() && in->eof())
			break;

		LineParser lp(line);
		lp.parse();

		validateCommand(lp);
		validateArguments(lp);

		// A partir de este punto ya tenemos la línea validada

		string cmd(lp.getCommand());

		// Cambia de estado si se pasa de leer Networkelement a Connection
		if (this->parsingState == STATE_NETWORKELEMENT
				&& cmd == CMD_CONNECTION) {
			this->parsingState = STATE_CONNECTION;
		}
		if (cmd == CMD_NETWORK_ELEMENT) {
			/**
			 * SPEC
			 *
			 * "NetworkElement: elemento de la red. Cada elemento consta de un
			 * nombre seguido de de su tipo (Hub, Node, Amp, CM)"
			 **/
			string tipo(lp.getParams()->at(1));
			string name(lp.getParams()->at(0));

			try {
				this->network->add(name, converCommandtToNetElemType(tipo));
			} catch (BaseTPException &e) {
				e.setFileName(this->fileName);
				e.setLineNum(this->line);
				throw e;
			}
		} else {
			/**
			 * SPEC
			 *
			 * Connection : usado para representar una relación de conexión
			 * entre elementos de red. Por ejemplo Connection X Y significa
			 * que el elemento X está conectado al elemento Y, y que Y es el
			 * padre de X
			 **/

			string child(lp.getParams()->at(0));
			string parent(lp.getParams()->at(1));

			try {
				this->network->connect(parent, child);
			} catch (BaseTPException &e) {
				e.setFileName(this->fileName);
				e.setLineNum(this->line);
				throw e;
			}
		}

		this->line++;
	};

	try {
		this->network->verify();
	} catch (BaseTPException &e) {
		e.setFileName(this->fileName);
		throw e;
	}
}

/**
 * Valida los argumentos pasados a los comandos
 *
 * precondición : lp->getCommand() es un comando válido
 */
void NetworkParser::validateArguments(const LineParser & lp) const
		throw (exception) {

	string cmd(lp.getCommand());

	if (lp.getParams()->size() != ELEMENT_OR_CONNECTION_PARAM_SIZE) {
		throw InvalidParametersException(cmd, this->line, ELEMENT_OR_CONNECTION_PARAM_SIZE,
				lp.getParams()->size(), this->fileName);
	}

	if (cmd == CMD_NETWORK_ELEMENT) {
		// NetworkElement
		string tipo(lp.getParams()->at(1));
		string nombre(lp.getParams()->at(0));

		// ToDo validar con un enum
		if (tipo != NETWORK_ELEMENT_HUB && tipo != NETWORK_ELEMENT_NODE
				&& tipo != NETWORK_ELEMENT_AMP && tipo != NETWORK_ELEMENT_CM) {
			throw WrongNetworkelementTypeException(tipo, this->line,
					this->fileName);
		}

	}
}

void NetworkParser::validateNetworkName(const LineParser & lp) const {
	string cmd = lp.getCommand();
	if (cmd != CMD_NETWORK_NAME) {
		throw WrongCommandException(cmd, CMD_NETWORK_NAME, this->line,
				this->fileName);
	}

	// SPEC: NetworkName : nombre de la red

	if (lp.getParams()->size() != NETWORKNAME_NUM_PARAMS) {
		throw InvalidParametersException(cmd, this->line, NETWORKNAME_NUM_PARAMS,
				lp.getParams()->size(), this->fileName);
	}
}

void NetworkParser::readNetworkName() throw (exception) {

	this->parsingState = STATE_NETWORKNAME;

	string line;
	getline(*in, line);

	LineParser lp(line);
	lp.parse();

	validateNetworkName(lp);

	/**
	 * SPEC : en la primera línea aparecerá la palabra NetworkName seguida del nombre de la red
	 */
	this->network->setName(lp.getParams()->at(0));
	this->line++;
}

