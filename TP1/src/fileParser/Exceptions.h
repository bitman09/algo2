/*
 * Exceptions.h
 *
 *  Created on: Apr 7, 2014
 */

#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <exception>
#include <string>
#include <sstream>
#include <cstring>

using namespace std;

class BaseTPException: public exception {
protected:
	static const int NO_LINE_NUMBER;
	static const char * NO_FILENAME;

	string filename;
	int line;
	mutable char * whatCStr;

	void fillWhat(ostringstream &) const;
public:
	BaseTPException();
	BaseTPException(const BaseTPException &e);
    const char * what() const throw ();
    void setLineNum(unsigned int);
    void setFileName(string);
	virtual ~BaseTPException() throw();

};

class WrongCommandException: public BaseTPException {
private:
	string cmd;
	string expected;
public:
	WrongCommandException(string , unsigned int, string );
	WrongCommandException(string , string, unsigned int, string );
    const char * what() const throw ();
	virtual ~WrongCommandException() throw();
};

class UnexpectedCommandException: public BaseTPException {
private:
	string cmd;
public:
	UnexpectedCommandException(string , unsigned int, string );
	virtual const char * what() const throw ();
	virtual ~UnexpectedCommandException() throw();
};

class InvalidParametersException: public BaseTPException {
private:
	string cmd;
	unsigned int expected;
	unsigned int obtained;
public:
	InvalidParametersException(string, unsigned int, unsigned int, unsigned int, string);
	virtual const char * what() const throw ();
	virtual ~InvalidParametersException() throw();
};

class WrongNetworkelementTypeException: public BaseTPException {
private:
	string elemType;
public:
	WrongNetworkelementTypeException(string , unsigned int, string );
	virtual const char * what() const throw ();
	virtual ~WrongNetworkelementTypeException() throw();
};

class NetworkElementNoDeclarado: public BaseTPException {
private:
	string name;
public:
	NetworkElementNoDeclarado(string);
	virtual const char * what() const throw ();
	virtual ~NetworkElementNoDeclarado() throw();
};


class DuplicatedNetworkElementException: public BaseTPException {
private:
	string name;
public:
	DuplicatedNetworkElementException(string);
	virtual const char * what() const throw ();
	virtual ~DuplicatedNetworkElementException() throw();
};

class MultipleHubsException: public BaseTPException {
public:
	MultipleHubsException();
	MultipleHubsException(unsigned int, string);
	virtual const char * what() const throw ();
	virtual ~MultipleHubsException() throw();
};

class DuplicatedConnectionException: public BaseTPException {
private:
	string father;
	string child;
public:
	DuplicatedConnectionException(string, string);
	virtual const char * what() const throw ();
	virtual ~DuplicatedConnectionException() throw();
};

class LoopException: public BaseTPException {
public:
	LoopException();
	virtual const char * what() const throw ();
	virtual ~LoopException() throw();
};

class NonFullyConnectedNetworkException: public BaseTPException {
public:
	NonFullyConnectedNetworkException();
	virtual ~NonFullyConnectedNetworkException() throw();
	virtual const char * what() const throw ();
};

class NoHubException: public BaseTPException {
public:
	NoHubException();
	virtual ~NoHubException() throw();
	virtual const char * what() const throw ();
};

class TwoParentsException: public BaseTPException {
private:
	string curentParent;
	string newParent;
	string child;
public:
	TwoParentsException(string, string, string);
	virtual ~TwoParentsException() throw();
	virtual const char * what() const throw ();
};

class HubNoInboundConnectionsException: public BaseTPException {
public:
	HubNoInboundConnectionsException();
	virtual ~HubNoInboundConnectionsException() throw();
	virtual const char * what() const throw ();
};

class CableModemNoOutboundConnectionsException: public BaseTPException {
public:
	CableModemNoOutboundConnectionsException();
	virtual ~CableModemNoOutboundConnectionsException() throw();
	virtual const char * what() const throw ();
};


#endif /* EXCEPTIONS_H_ */
