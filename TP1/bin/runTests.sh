#/bin/bash

__PRG=$(basename ${0})
__run="bin/tp1.exe"
__exec=${__run}
__isMemCheck=0
__testErrors=0
__testWarns=0
__testRun=0
__testMemLeaks=0
__tmp=/tmp/${__PRG}.$$

red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
cyan="$(tput setaf 6)"
off="$(tput sgr0)"

function usage() {
    cat <<EOT

   Test execution helper
   
   usage:
       $__PRG [--mem]
    
    mem : valgrind memory leaks detection
    
EOT

}

function showMsg() {
    echo -e "$__PRG: ${cyan}$1${off}"
}

function showErr() {
    showMsg "${red}ERROR:${off} $*"
}

function showWarn() {
    showMsg "${yellow}WARNING:${off} $*"
}

function build() {
	showMsg "Building..."
	make clean mem-check
}

function buildTests() {
	showMsg "Running tests..."
	make clean test && showPhase1Stats && bin/tp1-tests.exe
}

function showValgrindOrNot() {
	local __isMemCheck=$1
	local __outNoValgrind=$2
	local __out=$3
	
	if [[ ${__isMemCheck} -eq 1 ]]; then 
		if [[ `grep --count "ERROR SUMMARY: 0 errors from 0 contexts" ${__outErr}` -eq 1 ]]; then
			cat ${__outNoValgrind}
		else
			cat ${__out}
			showErr "Pérdida de memoria detectada" 
			let __testMemLeaks=__testMemLeaks+1
		fi
	else
		cat ${__outNoValgrind}
	fi
}

function showOutput() {
	showMsg "Output"
	local __outFile=$1
	local __outErr=$2
	local __outErrNoValgrind=$3
	local __isErrorResponse=$4
	
	if [[ ${__isErrorResponse} -eq 1 ]]; then
		## mostrar lo que hay en stderr
		showValgrindOrNot ${__isMemCheck} ${__outErrNoValgrind}  ${__outErr}
	else
		## mostrar el archivo pasado como -o
		local __tmpFile=${__tmp}/showOutput
		
		## Se debe mostrar la salida de valgrind + output 
		cat ${__outFile}  ${__outErr} > ${__tmpFile}
		showValgrindOrNot ${__isMemCheck} ${__outFile}  ${__tmpFile}
		rm -f ${__tmpFile}
	fi
}

function doCleanup() {
	rm -rf ${__tmp}
}

function init() {
	doCleanup
	mkdir -p ${__tmp}
}

function showPhase1Stats() {
	local __memLeaksResult=${__testMemLeaks}
	[[ ${__isMemCheck} -eq 0 ]] && __memLeaksResult="No se ejecutaron"
	
	showMsg "==== Fase 1   : estadísticas ===="
	showMsg "    Ejecutados : ${__testRun}"
	
	if [[ ${__testErrors} -eq 0 ]]; then
		showMsg "       Errores : ${__testErrors}"
	else
		showMsg "${red}       Errores : ${__testErrors}${off}"
	fi
	
	showMsg "    Mem. Leaks : ${__memLeaksResult}" 
	
	if [[ ${__testWarns} -eq 0 ]]; then
		showMsg "      Warnings : ${__testWarns}"
	else
		showMsg "${yellow}      Warnings : ${__testWarns}${off}" 
	fi
	
	showMsg "---------------------------------"
}

function doDiff() {
	local __isErrorResponse=$1
	local __gold=$2
	local __outErrNoValgrind=$3
	local __out=$4
	
	if [[ ${__isErrorResponse} -eq 1 ]]; then
		diff -U 3 ${__gold} ${__outErrNoValgrind}
	else
		diff -U 3 ${__gold} ${__out}
	fi
	
	if [[ $? -ne 0 ]]; then
		showErr "Salida incorrecta"
		let __testErrors=__testErrors+1
	fi
}

function doRunTests() {
	build
	
	[[ ! -x ${__exec} ]] && showErr "Error de compilación?" && exit -1
	
	for __inputFile in `ls tests/*.in`; do
		local __fileName=`basename ${__inputFile}`
		local __out="${__tmp}/${__fileName}.out" ## salida del programa
		local __gold=${__inputFile}.gold
		local __outErr=${__tmp}/${__fileName}.stderr
		local __outErrNoValgrind=${__outErr}.clean
		let __testRun=__testRun+1
		showMsg "------------------------------------------------------------------> ${__inputFile}"
		
		${__run} -i ${__inputFile} -o ${__out} >${__outErr} 2>&1
	
		## El gold file existe sólo si existe un error
		local __isErrorResponse=0
		[[ -f ${__gold} ]] && __isErrorResponse=1
		
		grep -v "^==" ${__outErr} > ${__outErrNoValgrind}
		
		showOutput ${__out} ${__outErr} ${__outErrNoValgrind} ${__isErrorResponse}
				
		showMsg "Diff"
		## si no es un error entonces el gold file es la entrada
		local __outSorted=${__out}.sorted	
		if [[ ${__isErrorResponse} -eq 0 ]]; then
			__gold=${__tmp}/${__fileName}.gold
			sort ${__inputFile} > ${__gold}
			sort ${__out} > ${__outSorted}
		fi
	
		doDiff $__isErrorResponse $__gold $__outErrNoValgrind $__outSorted

	done

	buildTests 
}

while [[ ! -z $1 ]]; do
    if [[ "$1" == "--mem" ]]; then
       shift
        __run="valgrind --leak-check=yes --track-origins=yes --read-var-info=yes ${__run}"
        __isMemCheck=1
    else
       showErr "Unknown option $1"
       usage
       exit -1
    fi
    shift
done

init
doRunTests
doCleanup
